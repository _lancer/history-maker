﻿Shader "Voxel/BlockLighting"
{
    Properties
    {
        [NoScaleOffset] _MainTex ("_MainTex", 2D) = "white" {}
        _ShadowMap ("_ShadowMap", 2D) = "white" {}
    }
    SubShader
    {
        Pass
        {
            CGPROGRAM
 
            // use "vert" function as the vertex shader
            #pragma vertex vert
            // use "frag" function as the pixel (fragment) shader
            #pragma fragment frag
 
            // vertex shader inputs
            struct appdata
            {
                float4 vertex : POSITION; // vertex position
                float2 uv : TEXCOORD0; // texture coordinate
            };
 
            // vertex shader outputs ("vertex to fragment")
            struct v2f
            {
                float2 pixelCoordinate : TEXCOORD1; //Location on shadow map
                float2 uv : TEXCOORD0; // texture coordinate
                float4 vertex : SV_POSITION; // clip space position
            };
 
            // vertex shader
            v2f vert (appdata v)
            {
                v2f o;
                // transform position to clip space
                // (multiply with model*view*projection matrix)
                o.vertex = UnityObjectToClipPos(v.vertex);
                // just pass the texture coordinate
                o.uv = v.uv;
 
                o.pixelCoordinate = float2 (0.5, 0.5);
                return o;
            }
           
            // texture we will sample
            sampler2D _MainTex;
            sampler2D _ShadowMap;
 
            // pixel shader; returns low precision ("fixed4" type)
            // color ("SV_Target" semantic)
            fixed4 frag (v2f i) : SV_Target
            {
                // sample texture and return it
                fixed4 col = tex2D(_MainTex, i.uv);
                fixed4 light = tex2D (_ShadowMap, float2 (0.5, 0.5));
                return col * light * 0.5;
            }
            ENDCG
        }
    }
}