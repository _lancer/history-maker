﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlatModel : MonoBehaviour
{
    public GameObject planet;

    public void makeShadowFlatMap()
    {
        foreach (var cell in Global.grid.Values)
        {
            if (cell != null)
            {
                GameObject node = Instantiate(cell);
                node.transform.rotation = Quaternion.Euler(90, 0, 0);
                node.transform.position = ToSpherical(node.transform.position - planet.transform.position);
            }
        }
    }

    Vector3 ToSpherical(Vector3 aCartesian)
    {
        return ToSpherical(aCartesian, Quaternion.identity);
    }

    Vector3 ToSpherical(Vector3 aCartesian, Quaternion aSpace)
    {
        var q = Quaternion.Inverse(aSpace);
        var len = aCartesian.magnitude;
        var v = q * aCartesian / len;
        Vector3 result;
        result.x = Mathf.Atan2(v.z, v.x);
        result.y = Mathf.Asin(v.y);
        result.z = len;
        return result;
    }

    Vector3 ToCartesian(Vector3 aSpherical)
    {
        return ToCartesian(aSpherical, Quaternion.identity);
    }

    Vector3 ToCartesian(Vector3 aSpherical, Quaternion aSpace)
    {
        Vector3 result;
        float c = Mathf.Cos(aSpherical.y);
        result.x = Mathf.Cos(aSpherical.x) * c;
        result.y = Mathf.Sin(aSpherical.y);
        result.z = Mathf.Sin(aSpherical.x) * c;
        result *= aSpherical.z;
        result = aSpace * result;
        return result;
    }
}
