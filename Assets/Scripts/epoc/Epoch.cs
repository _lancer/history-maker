﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Epoch
{

    public GameObject ui;
    int points = 0;

    public List<Vector3> discoveredCells = new List<Vector3>();
    public List<Player> contacts = new List<Player>();

    public static int stoneAge = 0;

    public static int[] limits =  new int[] { 100, 200, 300, 400, 500, 1000 };

    int required = limits[0];

    public void discoverCell (Vector3 cell)
    {
        if (discoveredCells.Contains(cell)) return;
        points += 3;
        discoveredCells.Add(cell);

        updateUI();

    }

    public void hire ()
    {
        points -= 1;
        updateUI();

    }

    public void buildTown ()
    {
        points += 3;
        updateUI();

    }

    public void buildFort()
    {
        points += 10;
        updateUI();

    }


    public void buildShip()
    {
        points += 6;
        updateUI();

    }

    public void contact (Player player)
    {
        if (contacts.Contains(player)) return;
        points += 20;
        contacts.Add(player);
        updateUI();

    }

    public void dayily ()
    {
        points += 1;
        points += contacts.Count;
        updateUI();
    }

   public void updateUI ()
    {
        float x = (float) points /  (float) required;
        if (x < 0) x = 0;
        if (x > 1) x = 1;
        ui.transform.localScale = new Vector3(x, 1, 1);


        newAge();
    }

    public void captureTown ()
    {
        points += 3;
        updateUI();
    }

    public static string[] epochnames = new string[] { "stone age", "antique", "medieval", "renessanse", "modern", "new" };
    public bool secondEpoch = false;
    public void newAge()
    {
        return;
        /*
        if (points < required) return;

        Global.inst.localPlayer.epoch++;

        if (Global.inst.localPlayer.epoch >= 1)
            Global.inst.boatButton.SetActive(true);

        if(Global.inst.localPlayer.epoch >= 2)
            Global.inst.buildFortButton.SetActive(true);


        if (Global.inst.localPlayer.epoch > 4) return;

        Global.inst.townIcon.GetComponent<UnityEngine.UI.Image>().sprite 
            = Global.inst.townIcons[Global.inst.localPlayer.epoch];

        Global.inst.armyIcon.GetComponent<UnityEngine.UI.Image>().sprite 
            = Global.inst.armyIcons[Global.inst.localPlayer.epoch];

        Global.inst.ladIcon.GetComponent<UnityEngine.UI.Image>().sprite
            = Global.inst.ladIcons[Global.inst.localPlayer.epoch];

        Global.inst.boatIcon.GetComponent<UnityEngine.UI.Image>().sprite
           = Global.inst.boatIcons[Global.inst.localPlayer.epoch];

        Global.inst.localPlayer.towns.ForEach(t =>
        {
            for (int i = 0; i < Global.towns[t].transform.childCount; i++ )
            {
                Global.towns[t].transform.GetChild(i).gameObject.SetActive(i == Global.inst.localPlayer.epoch);
            }
        });

        Global.inst.localPlayer.units.ForEach(t =>
        {
            t.epochUpgrade(Global.inst.localPlayer.epoch);
        });

        Global.inst.epochName.text = epochnames[Global.inst.localPlayer.epoch];

        points = 0;
        required = limits[Global.inst.localPlayer.epoch];

        float x = (float)points / (float)required;
        if (x < 0) x = 0;
        if (x > 1) x = 1;
        ui.transform.localScale = new Vector3(x, 1, 1);
        */
    }
}
