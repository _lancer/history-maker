﻿using System.Collections.Generic;
using UnityEngine;

public class CullSprites : MonoBehaviour
{

    Transform planet;
    Transform cam;
    List<Renderer> rends = new List<Renderer>();

    private void Start()
    {
        planet = Global.inst.planet.transform;
        cam = Camera.main.transform;
        rends.AddRange(GetComponentsInChildren<Renderer>());
    }


    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 planetToCam = planet.position - cam.position;
        Vector3 thisToCam = transform.position - cam.position;
        float d = Vector3.Distance(Vector3.zero, planetToCam) - Vector3.Distance(Vector3.zero, thisToCam);
        rends.ForEach(r => r.enabled = (d > 0));
    }
}
