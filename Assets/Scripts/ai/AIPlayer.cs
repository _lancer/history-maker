﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIPlayer : MonoBehaviour
{

    public Player playerRef;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    float timer = 2;
    // Update is called once per frame
    void FixedUpdate()
    {
        if (timer <= 0)
        {
            SpendMoney();
            CaptureCells();
            timer = Global.oneDayTimeUnit * 1.1f;
        } else
        {
            timer -= Time.deltaTime;
        }
    }

    void CaptureCells ()
    {
        foreach (Army army in playerRef.armies)
        {
            if (army.inFight) continue;
            army.GetComponent<Movement>().ai = this;
            List<Vector3> n = ShowRange(army);
            if (n.Count == 0) continue;
            Vector3 got_o = n[Random.Range(0, n.Count)];
            army.GetComponent<Movement>().toPay
                = Unit.MovementCost(Global.landscape[got_o]);
            army.GetComponent<Movement>().target = Global.grid[got_o].transform.GetChild(3);

        }
    }

    int turn = 0;
    void SpendMoney ()
    {
        if (playerRef.territory.Count == 0)
        {
            gameObject.SetActive(false);
            return;
        }
        foreach (Vector3 ter in playerRef.territory)
        {
            if (playerRef.towns.Contains(ter))
                continue;
            else
            {
                Ui.inst.BuildTown(ter, playerRef);
                break;
            }
        }
        if (Economy.GetInfl(playerRef) < 0) return;
        foreach (Army army in playerRef.armies)
        {
            if (army.UnitsCount() == 9 || army.inFight) continue;
            else
            {
                addUnit(army);
                break;
            }
        }
        if (turn % 5 == 1 || playerRef.armies.Count == 0 && playerRef.towns.Count > 0)
        {
            Army army = Ui.inst.AddArmy(playerRef.towns[playerRef.towns.Count - 1], playerRef);
            addUnit(army);
        }
        turn++;
    }

    public void ArmyMovementDone (Army army)
    {
        if (army.GetComponent<Unit>().currentMovement <= 0 || army.inFight) return; 
        List<Vector3> n = ShowRange(army);
        if (n.Count == 0) return;
        Vector3 got_o = n[Random.Range(0, n.Count)];

        army.GetComponent<Movement>().toPay
            = Unit.MovementCost(Global.landscape[got_o]);
        army.GetComponent<Movement>().target = Global.grid[got_o].transform.GetChild(3);
    }
    public List<Vector3> ShowRange(Army army)
    {

        List<Vector3> r = new List<Vector3>();

        Vector3 position = army.GetComponent<Movement>().target_.transform.parent.localPosition;

        List<Vector3> near = Global.nodes[position].near;

        foreach (Vector3 p in near)
        {
            if (Global.grid[p] != null)
            {
                // area availability movement points
                if (!army.GetComponent<Unit>().CanMoveTo(Global.landscape[p], p))
                {
                    continue;
                }
                r.Add(p);
            }
        }
        return r;
    }

    public void addUnit(Army army)
    {
      
        Unit u = Ui.inst.AddUnit(army.GetComponent<Movement>().target_.parent.localPosition, false);
        u.owner = army.GetComponent<Unit>().owner;

        u.transform.GetChild(0).GetComponent<SpriteRenderer>().color =
            army.GetComponent<Unit>().owner.color;


        u.GetComponent<Unit>().armyRef = army;

        army.AddUnit(u);
    }
}
