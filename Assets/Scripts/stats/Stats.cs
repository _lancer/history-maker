﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Stats : MonoBehaviour
{

    public GameObject menu;
    public GameObject gameMenu;
    public Text text;
    public Text winLose;
    public static Stats inst;

    public int territories;
    public int villages;
    public int money;
    public int unitsHired;
    public int unitsKilled;
    public int unitsLost;

   public Admob admob;
    // Start is called before the first frame update
    void Start()
    {
        inst = this;
    }

    public void Show (bool win)
    {

        admob.Show();

        gameMenu.SetActive(false);
        winLose.text = "STATS\nYOU WON";
        if (!win)
            winLose.text = "STATS\nYOU LOST";

        text.text = "territores \t" + Global.inst.localPlayer.territory.Count + "\n" +
            "villages \t" + Global.inst.localPlayer.towns.Count + "\n" +
            "money earned \t" + money + "\n" +
            "units hired \t" + unitsHired + "\n" +
            "units killed \t" + unitsKilled + "\n" +
            "units lost \t" + unitsLost + "\n";
        menu.SetActive(true);
    }
}
