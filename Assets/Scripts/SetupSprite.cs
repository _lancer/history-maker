﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetupSprite : MonoBehaviour
{
    Material mat;
    float dist = 6;
    // Start is called before the first frame update
    void Start()
    {
        mat = GetComponent<SpriteRenderer>().material;
    }

    // Update is called once per frame
    public void FixedUpdate()
    {
        //3 white
        //-3 black
        float distance = 1.3f - Vector3.Distance(Global.inst.sun.transform.position, transform.position) / dist;
        Color color = new Color(distance, distance, distance);
        mat.color = color;
    }
}
