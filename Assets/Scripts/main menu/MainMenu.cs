﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
            Time.timeScale = 0;
     
    }

    public GameObject[] activateOnStart;

    public GameObject menu;


    public void totorial ()
    {
        Tutorial.tutorialMode = true;
        Tutorial.InitiateTutorial();
        menu.SetActive(false);
        Time.timeScale = 1;

    }
    public void recreate ()
    {
        Application.LoadLevel(0);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void Play()
    {
        menu.SetActive(false);
        foreach (GameObject g in activateOnStart)
        {
            g.SetActive(true);
        }
        Time.timeScale = 1;

        Global.inst.CheckForVictory();
    }
}
