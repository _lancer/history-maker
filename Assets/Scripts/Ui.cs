﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ui : MonoBehaviour
{
    public static Ui inst;

    public GameObject buildings_menu;
    private void Start()
    {
        inst = this;
        Ui.unselect = null;
        UpdateInfl();
    }

    public void UpdateInfl ()
    {
        int ec = Economy.GetInfl(Global.inst.localPlayer);
        string expected_cahnge = " (" + (ec > 0 ? "+" : "") + ec + ")";
        Ui.inst.influenceAmount.text = "" + Global.inst.localPlayer.influence + expected_cahnge;
    }
    public GameObject army;

    public void addUnitSelectedArmy()
    {
        if (Selectable.firstSelected.GetComponent<Army>().UnitsCount() == 9)
        {
            SoundManager.inst.nope.Play();
            InfoMessage.inst.Show("Max capacity is 9");
            return;
        }
        if (Global.inst.localPlayer.influence - Economy.GetUnitPrice() < 0)
        {
            SoundManager.inst.nope.Play();
            InfoMessage.inst.Show("Not enough money");
            return;

        }

        Stats.inst.unitsHired += 1;
        Global.inst.localPlayer.influence -= Economy.GetUnitPrice();
        Unit u = AddUnit(Selectable.firstSelected.GetComponent<Movement>().target_.parent.localPosition);
        u.owner = Selectable.firstSelected.GetComponent<Unit>().owner;

        u.transform.GetChild(0).GetComponent<SpriteRenderer>().color =
            Selectable.firstSelected.GetComponent<Unit>().owner.color;


        u.GetComponent<Unit>().armyRef = Selectable.firstSelected.GetComponent<Army>();

        Selectable.firstSelected.GetComponent<Army>().AddUnit(u);

        Global.inst.localPlayerEpoch.hire();

        SoundManager.inst.lad.Play();

    }
    public Army AddArmy(Vector3 position, Player for_)
    {
        GameObject clone = Instantiate(army,
            Global.inst.planet.transform.TransformPoint(position), Quaternion.identity);

        clone.transform.up = Global.inst.planet.transform.TransformPoint(position);
        clone.transform.parent = Global.inst.planet.transform;
        clone.transform.localPosition = (position);
        //clone.transform.localPosition = clone.transform.localPosition.normalized * Global.unitDist;
        CharactersProxy.inst.Barbarian(clone);
        clone.GetComponent<Unit>().owner = for_;

        clone.transform.GetChild(0).GetComponent<SpriteRenderer>().color = for_.color;

        clone.GetComponent<Movement>().target_ = Global.grid[position]
                .GetComponentInChildren<DEBUG_OnClieckChangeBorder>().transform;

        Global.AddUnit(position, clone.GetComponent<Unit>());

      //  for_.units.Add(clone.GetComponent<Unit>());
        for_.armies.Add(clone.GetComponent<Army>());
        // mountain fix
        if (Global.landscape[position].type == Global.LandscapeData.Type.mountain)
        {
            clone.transform.localPosition += clone.transform.localPosition.normalized * Movement.mtUp;
            clone.GetComponent<Movement>().onMountain = true;
        }

        Ui.inst.UpdateInfl();

        return clone.GetComponent<Army>();
    }
    public Unit AddUnit (Vector3 position, bool localpl = true)
    {
        GameObject clone = Instantiate(CharactersProxy.inst.barbarian, 
            Global.inst.planet.transform.TransformPoint (position), Quaternion.identity);
        
        clone.transform.up = Global.inst.planet.transform.TransformPoint(position);
        clone.transform.parent = Global.inst.planet.transform;
        clone.transform.localPosition = (position);

        clone.GetComponent<Movement>().target_ = Global.grid[position]
                .GetComponentInChildren<DEBUG_OnClieckChangeBorder>().transform;

        Global.AddUnit(position, clone.GetComponent<Unit>());

        if (localpl)
            Global.inst.localPlayer.units.Add(clone.GetComponent<Unit>());

        // mountain fix
        if (Global.landscape[position].type == Global.LandscapeData.Type.mountain)
        {
            clone.transform.localPosition += clone.transform.localPosition.normalized * Movement.mtUp;
            clone.GetComponent<Movement>().onMountain = true;
        }


        Ui.inst.UpdateInfl();

        clone.transform.GetChild(1).GetChild(0).gameObject.GetComponent<Collider>().enabled = (true);
        clone.transform.GetChild(0).GetComponent<SpriteRenderer>().color = Global.inst.localPlayer.color;

        return clone.GetComponent<Unit>();
    }

    public Text townPrice;
    public void BuildTown (Vector3 at, Player player)
    {
        if (player.influence - Economy.GetTownPrice(player) < 0)
        {
            SoundManager.inst.nope.Play();
            InfoMessage.inst.Show("Not enough  money");

            return;
        }

        player.influence -= Economy.GetTownPrice(player);

        GameObject t = Instantiate(Global.inst.townPrototype, at, Quaternion.identity);
        t.transform.parent = Global.grid[at].transform;
        t.transform.localPosition = Vector3.zero;
        t.transform.localRotation = Quaternion.identity;
        player.towns.Add(at);
        Ui.inst.UpdateInfl();
        if (Global.landscape[at].type == Global.LandscapeData.Type.mountain)
        {
         //   print(t.transform.localPosition);
            t.transform.localPosition += Global.grid[at].transform.localPosition.normalized 
                * (Movement.mtUp*5);
           // print(t.transform.localPosition);
        }

        Global.towns[at] = t;
        if (player == Global.inst.localPlayer)
        {
            townPrice.text = Economy.GetTownPrice(Global.inst.localPlayer) + "";
            Global.inst.localPlayerEpoch.buildTown();
        }

        for (int i = 0; i < t.transform.childCount; i++)
        {
            t.transform.GetChild(i).gameObject.SetActive(i == player.epoch);
        }
        if (player == Global.inst.localPlayer)
            SoundManager.inst.Build.Play();

        //buildTown ()
    }


    public GameObject doneButton;
    public static Selectable.I unselect;
    public void done ()
    {
        if (Build.inProgress)
        {
            Build.inst.StopBuilding();
            return;
        }
        if (Hire.inProgress)
        {
            Hire.inst.StopHiring();
            return;
        }
        if (MkBoat.inProgress)
        {
            MkBoat.inst.StopHiring();
            return;
        }
        if (mkFort.inProgress)
        {
            mkFort.inst.StopHiring();
            return;
        }

        inst.buildings_menu.SetActive(false);
        if (unselect != null)
        unselect.unselect();
    }

    public Text influenceAmount;



}
