﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{

    public GameObject armyMenu;
    public GameObject totalOk;

    public GameObject gameMenu;

    public GameObject hire;
    public GameObject house;
    public GameObject boat;

    public Controls controls;

    public static bool tutorialMode = false;

    public static List<AIPlayer> ai;

    public GameObject king;
    public Text kingsWord;

    public Button okButton;

    public static Tutorial inst;
    private void Awake()
    {
        ai = new List<AIPlayer>();
        okButton.onClick.AddListener(next0);
        inst = this;
    }
    // Start is called before the first frame update

    public static void InitiateTutorial ()
    {
        foreach (AIPlayer p in ai)
            p.enabled = false;

        inst.controls.enabled = false;

        inst.Invoke("run", .5f);

        Global.inst.localPlayer.influence = 1000;
    }

    public void run ()
    {
        say("Hi I'm King");
        king.SetActive(true);
    }

    public void say (string words)
    {
        kingsWord.text = words;
    }
    public void next0 ()
    {
        say("I'll teach you how to gowern people");
        okButton.onClick.RemoveListener(next0);
        okButton.onClick.AddListener(next1);
    }
    public void next1()
    {
        say("Let's begin !");
        okButton.onClick.RemoveListener(next1);
        okButton.onClick.AddListener(next2);
    }

    public void next2()
    {
        say("Let's begin !");
        next3();
        okButton.onClick.RemoveListener(next2);
    }

    public void next3()
    {
        centerCamera(Global.inst.localPlayer.home);
        king.SetActive(false);
        inst.Invoke("next3_1", 1f);
        // say("Let's begin !");
        /*
        okButton.onClick.RemoveListener(next2);
        okButton.onClick.AddListener(next3);
        */
    }

    public void next3_1()
    {
        say("This is our town and army");
        king.SetActive(true);
        okButton.onClick.AddListener(next4);
    }

    public void next4()
    {
        say("Let's talk to the army (select army)");

        okButton.onClick.RemoveListener(next4);
        okButton.onClick.AddListener(next5);
    }

    public void next5()
    {
        king.SetActive(false);
        inst.controls.enabled = true;

        StartCoroutine (waitForArmySelect());

    }

    IEnumerator waitForArmySelect()
    {
        print("wating for select");
        yield return new WaitUntil(() => Selectable.firstSelected != null);
        next6();
    }
    public void next6()
    {

        inst.controls.enabled = false;
        armyMenu.SetActive(false);
        totalOk.SetActive(false);
        say("EXCELLENT !");
        king.SetActive(true);

        okButton.onClick.RemoveListener(next5);
        okButton.onClick.AddListener(next7);

    }

    public void next7 ()
    {
        say("Say the boys move somwhere ");
        okButton.onClick.RemoveListener(next7);
        okButton.onClick.AddListener(next8);
    }
    public void next8()
    {
        king.SetActive(false);
        inst.controls.enabled = true;

        okButton.onClick.RemoveListener(next8);
        StartCoroutine(waitForMovementStrat());
    }

    IEnumerator waitForMovementStrat()
    {
        yield return new WaitUntil(() => !Selectable.firstSelected.GetComponent<Movement>().movementDone);
        StartCoroutine(waitForMovementEnd());
    }

    IEnumerator waitForMovementEnd()
    {
        yield return new WaitUntil(() => Selectable.firstSelected.GetComponent<Movement>().movementDone);
        next9();
    }

    public void next9()
    {

        inst.controls.enabled = false;
        armyMenu.SetActive(false);
        totalOk.SetActive(false);
        say("GREAT JOB !");
        king.SetActive(true);

        okButton.onClick.RemoveListener(next8);
        okButton.onClick.AddListener(next10);

    }
    public void next10()
    {
        say("We expanded territory and now earning more influence (money)");
        okButton.onClick.RemoveListener(next10);
        okButton.onClick.AddListener(next11);
    }

    public void next11()
    {
        say("Capture more territories");
        okButton.onClick.RemoveListener(next11);
        okButton.onClick.AddListener(next11end);
    }
    public void next11end()
    {
        king.SetActive(false);
        inst.controls.enabled = true;

        okButton.onClick.RemoveListener(next11end);
        StartCoroutine(waitForArmyMovement());
    }
    IEnumerator waitForArmyMovement()
    {
        yield return new WaitUntil(() => Selectable.firstSelected.GetComponent<Unit>().currentMovement_ <=0);
        next12();
    }

    public void next12()
    {

        inst.controls.enabled = false;
        armyMenu.SetActive(false);
        totalOk.SetActive(false);
        say("FANTASTIC !");
        king.SetActive(true);


        okButton.onClick.AddListener(next13);

    }

    public void next13()
    {
        say("Boys are tired. Let them rest ");
        okButton.onClick.RemoveListener(next13);
        okButton.onClick.AddListener(next14);
    }

    public void next14()
    {
        say("Let's build new village now");
        okButton.onClick.RemoveListener(next14);;
        okButton.onClick.AddListener(next14end);;
    }

    public void next14end()
    {
        king.SetActive(false);
        inst.controls.enabled = true;

        Selectable s = Selectable.firstSelected;
        Selectable.firstSelected.unselect();

        s.enabled = false;

        totalOk.SetActive(false);

        gameMenu.SetActive(true);
        hire.SetActive(false);
        boat.SetActive(false);
        house.SetActive(true);

        okButton.onClick.RemoveListener(next14end);
        StartCoroutine(wiatForTown());
    }

    IEnumerator wiatForTown()
    {
        int current = Global.inst.localPlayer.towns.Count;
        yield return new WaitUntil(() => Global.inst.localPlayer.towns.Count > current);
        next15();
    }

    public void next15()
    {

        inst.controls.enabled = false;
        armyMenu.SetActive(false);
        totalOk.SetActive(false);
        say("OUTSTANDING !");
        king.SetActive(true);

        okButton.onClick.AddListener(next16);

    }

    public void next16 ()
    {
        say("Now.");

        okButton.onClick.RemoveListener(next16); ;
        okButton.onClick.AddListener(next17);
    }

    public void next17()
    {
        say("To expand territory we need more scouts.");

        okButton.onClick.RemoveListener(next17); ;
        okButton.onClick.AddListener(next18);
    }

    public void next18()
    {
        say("Hire one more army");

        okButton.onClick.RemoveListener(next18); ;
        okButton.onClick.AddListener(next18end); ;
    }

    public void next18end()
    {
        king.SetActive(false);
        inst.controls.enabled = true;

        Ui.inst.done();

        totalOk.SetActive(false);

        gameMenu.SetActive(true);
        hire.SetActive(true);
        boat.SetActive(false);
        house.SetActive(true);
        house.GetComponent<Button>().enabled = false;

        okButton.onClick.RemoveListener(next18end);
        StartCoroutine(waitForHire());
    }

    IEnumerator waitForHire()
    {
        int current = Global.inst.localPlayer.armies.Count;
        yield return new WaitUntil(() => Global.inst.localPlayer.armies.Count > current);
        next19();
    }
    public void next19()
    {

        inst.controls.enabled = false;
        armyMenu.SetActive(false);
        totalOk.SetActive(false);
        say("YOU ARE THE BEST !");
        king.SetActive(true);

        okButton.onClick.AddListener(next20);

    }
    // hire lads
    public void next20()
    {
        say("Let's turn our scout to an iron FIST");

        okButton.onClick.RemoveListener(next20); ;
        okButton.onClick.AddListener(next21); ;
    }

    public void next21()
    {
        say("Pick an army and hire 8 more lads");

        okButton.onClick.RemoveListener(next21); ;
        okButton.onClick.AddListener(next21end); ;
    }

    public void next21end()
    {
        king.SetActive(false);
        inst.controls.enabled = true;

        Ui.inst.done();

        totalOk.SetActive(false);

        gameMenu.SetActive(true);
        hire.SetActive(true);
        boat.SetActive(false);
        house.SetActive(true);
        house.GetComponent<Button>().enabled = false;
        hire.GetComponent<Button>().enabled = false;

        okButton.onClick.RemoveListener(next21end);
        StartCoroutine(waitForArmySelect2());
    }
    IEnumerator waitForArmySelect2()
    {
        yield return new WaitUntil(() => Selectable.firstSelected != null);
        armyMenu.SetActive(true);
        totalOk.SetActive(false);
        StartCoroutine(waitForHire9Lads());
    }
    IEnumerator waitForHire9Lads()
    {
        yield return new WaitUntil(() => Selectable.firstSelected.GetComponent<Army>().units.Count == 9);
        next22();
    }

    public void next22()
    {

        inst.controls.enabled = false;
        armyMenu.SetActive(false);
        totalOk.SetActive(false);
        say("BRILLIANT !");
        king.SetActive(true);

        okButton.onClick.AddListener(next23);

    }

    public void next23()
    {
        say("Hm. Who is this");

        createEnemyNeatToPlayerHome();

        okButton.onClick.RemoveListener(next23); 
        okButton.onClick.AddListener(next24); ;
    }

    public void next24()
    {
        centerCamera(Global.inst.localPlayer.home);
        king.SetActive(false);
        print("Invoke 25");
        Invoke("next25", 1f);
    }

    public void next25()
    {
        say("Look ! Enemy army !");
        king.SetActive(true);
        okButton.onClick.RemoveListener(next24);
        okButton.onClick.AddListener(next26);
    }

    public void next26()
    {
        say("Destroy them !");

        okButton.onClick.RemoveListener(next26); ;
        okButton.onClick.AddListener(next26end); ;
    }
    public void next26end()
    {
        king.SetActive(false);
        inst.controls.enabled = true;

        Ui.inst.done();

        totalOk.SetActive(false);

        gameMenu.SetActive(true);
        hire.SetActive(true);
        boat.SetActive(false);
        house.SetActive(true);
        house.GetComponent<Button>().enabled = false;
        hire.GetComponent<Button>().enabled = true;

        okButton.onClick.RemoveListener(next26end);
        StartCoroutine(waitUntilVictory());
    }
    IEnumerator waitUntilVictory()
    {
        yield return new WaitUntil(() => !enemyArmy.activeSelf);
        final();
    }

    public void final ()
    {
        if (Selectable.firstSelected != null)
        Selectable.firstSelected.unselect();
        Selectable.firstSelected = null;
        inst.controls.enabled = false;
        armyMenu.SetActive(false);
        totalOk.SetActive(false);
        say("AMAZING !");
        king.SetActive(true);

        okButton.onClick.AddListener(final2);
    }

    public void final2()
    {
        say("We are about done");

        okButton.onClick.RemoveListener(final2); ;
        okButton.onClick.AddListener(final3); ;
    }
    public void final3()
    {
        say("There is an ability to build boats");

        okButton.onClick.RemoveListener(final3); ;
        okButton.onClick.AddListener(final4); ;
    }

    public void final4()
    {
        say("...and travel overseas");

        okButton.onClick.RemoveListener(final4);
        okButton.onClick.AddListener(final5);
    }

    public void final5()
    {
        say("But. It is time for me to leave");

        okButton.onClick.RemoveListener(final5);
        okButton.onClick.AddListener(final6);
    }

    //set dead king
    public void final6()
    {
        say("And for YOU to become the KING");

        okButton.onClick.RemoveListener(final6);
        okButton.onClick.AddListener(final7);
    }

    public GameObject deadEyes;
    public AudioSource boink;
    //set dead king
    public void final7()
    {
        say("...");
        boink.Play();
        deadEyes.SetActive(true);
        Invoke("final8", 1f);
        okButton.onClick.RemoveListener(final7);
    }

    //end tutorial
    public GameObject endTutorialButton;
    public void final8()
    {
        endTutorialButton.SetActive(true);
    }

    public void EndTutorial ()
    {
        tutorialMode = false;
        Application.LoadLevel(0);
    }
    GameObject enemyArmy;
    public Vector3 createEnemyNeatToPlayerHome ()
    {
        Vector3 point = Vector3.zero;
        foreach (Vector3 v in Global.inst.getNearest(Global.inst.localPlayer.home))
        {
            if (Global.landscape[v].type != Global.LandscapeData.Type.water)
            {
                point = v;
                break;
            }
        }

        Army army = Ui.inst.AddArmy(point, Global.inst.players[1]);
        addUnit(army);
        addUnit(army);
        addUnit(army);
        addUnit(army);
        enemyArmy = army.gameObject;

        return point;
    }
    public void addUnit(Army army)
    {

        Unit u = Ui.inst.AddUnit(army.GetComponent<Movement>().target_.parent.localPosition, false);
        u.owner = army.GetComponent<Unit>().owner;

        u.transform.GetChild(0).GetComponent<SpriteRenderer>().color =
            army.GetComponent<Unit>().owner.color;


        u.GetComponent<Unit>().armyRef = army;

        army.AddUnit(u);
    }
    public void centerCamera(Vector3 targetPosition)
    {
        Global.inst.planet.transform.rotation = Quaternion.FromToRotation(targetPosition, Camera.main.transform.position);
        Global.inst.planet.transform.Rotate(10f, 0, 0);

    }
}
