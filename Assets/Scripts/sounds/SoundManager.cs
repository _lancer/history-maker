﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour
{
    public GameObject canvas;
     Button[] buttons;
    public AudioSource clickSound;

    public AudioSource nope;
    public AudioSource army;
    public AudioSource lad;
    public AudioSource Build;
    public AudioSource Boat;
    public static SoundManager inst;
    // Start is called before the first frame update
    void Start()
    {
        buttons = canvas.GetComponentsInChildren<Button>(true);
        foreach (Button b in buttons)
            b.onClick.AddListener(PlayClick);

        inst = this;
    }

    public void PlayClick ()
    {
        clickSound.Stop();
        clickSound.Play();
    }

    public AudioMixer uiMixer;
    public AudioMixer musicMixer;
    public AudioMixer envMixer;

    public void SetLevelUI(float sliderValue)
    {
        uiMixer.SetFloat("ui-mixer-vol", Mathf.Log10(sliderValue) * 20);
    }

    public void SetLeveMusic(float sliderValue)
    {
        musicMixer.SetFloat("music-mixer-vol", Mathf.Log10(sliderValue) * 20);
    }

    public void SetLeveEnv(float sliderValue)
    {
        envMixer.SetFloat("env-mixer-vol", Mathf.Log10(sliderValue) * 20);
    }


}
