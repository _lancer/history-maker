﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGenerator : MonoBehaviour
{

    public GameObject planet;
    public GameObject plainPrototype;
    public GameObject hillPrototype;
    public GameObject mtnPrototype;
    public GameObject icePrototype;


    public GameObject waterPrototype;


    public GameObject cactFusorestPrototype;
    public GameObject palmForestPrototype;
    public GameObject palmPoorForestPrototype;
    public GameObject treeForestPrototype;
    public GameObject firForestPrototype;

    public GameObject townPrototype;

    public GameObject borderProto;

    public Material sandMaterial;
    /*
    public Texture sandTexture;
    public Color sandColor;
 */
    // Start is called before the first frame update
    void Start()
    {
       // Random.seed = 3;

        Vector3[] points = planet.GetComponent<MeshFilter>().mesh.vertices;

        HashSet<Vector3> uniq = new HashSet<Vector3>(points);

       // createGrid();
        List<Vector3> keys = pickKeyPoints();


        Vector3 home = Vector3.one*100;
        int i = 0;
        foreach (Vector3 p in uniq)
        {
            Global.grid.Add(p, null); 
            Global.landscape.Add(p, null); 
             
            bool snow = false;
            bool sand = false;
            bool town = false;

            bool forest = false;
            bool mountain = false;
            // SNOW
            if (Vector3.Angle (p,Vector3.up) < 20 || Vector3.Angle(p, Vector3.up) > 160)
            {
                if (Random.value < .75f)
                {
                    snow = true;
                    GameObject clone = Instantiate(icePrototype, p, Quaternion.identity);
                    clone.transform.up = p;
                    clone.transform.RotateAround(clone.transform.up, Random.Range(0, 360));
                    clone.transform.parent = planet.transform;
                }
            }
            if (keys.Contains (p) || (chances (nearestKey (p, keys)) > Random.value))
            {
               
                GameObject toClone = plainPrototype;
                // OBJECT ITSELF
                GameObject clone = Instantiate(toClone, p, Quaternion.identity);
                clone.transform.up = p;
                float plain_rotation = Random.Range(0, 360);
                clone.transform.RotateAround(clone.transform.up, plain_rotation);
                clone.transform.parent = planet.transform;
                //clone.transform.GetChild(2).RotateAround(clone.transform.up, -plain_rotation);
                Global.grid[p] = clone;
                Global.units[p] = null;
                Global.towns[p] = null;
                  
                //ADD TOWN
                if (keys.Contains(p))
                {
                    //CharactersProxy.inst.barbarian
                    town = true;
                    GameObject t = Instantiate(townPrototype, p, Quaternion.identity);
                    t.transform.parent = clone.transform;
                    t.transform.localPosition = Vector3.zero;
                    t.transform.localRotation = Quaternion.identity;

                    Global.towns[p] = t;

                    if (home == Vector3.one * 100)
                    {
                        home = p;
                    }
                    if (i < Global.inst.playerColors.Length)
                   // if (i == 0)
                    {
                        
                        clone.transform.GetChild(2).gameObject.SetActive(true);
                        clone.transform.GetChild(2).gameObject.GetComponent<Renderer>().material
                            .SetColor ("Color_A834058F", Global.inst.players[i].color);
                        
                        Global.inst.players[i].territory.Add(p);
                        Global.inst.players[i].home = p;

                        Global.inst.players[i].town = t.GetComponent<Town>();
                        Global.inst.players[i].towns.Add(p);


                        //   Global.inst.players[i] .merge();

                        GameObject u = addUnit(p, i, clone);

                        if (i == 0)
                        {
                            Global.AddTownButton(t);
                        }

                        Global.inst.players[i].units.Add(u.GetComponent<Unit>());

                        u.transform.GetChild(0).GetComponent<SpriteRenderer>().color = Global.inst.players[i].color;

                        Army a = Global.inst.AddArmy(p, Global.inst.players[i]);

                        a.AddUnit(u.GetComponent<Unit>());

                        u.GetComponent<Unit>().armyRef = a;
                        Global.inst.players[i].armies.Add(a);

                        clone.GetComponentInChildren<DEBUG_OnClieckChangeBorder>().owner
                            = Global.inst.players[i];

                        //u.GetComponent<Movement> ().target_ = 
                        
                        if (i != 0)
                        {
                           GameObject g= Instantiate(Global.inst.aiPrototype);
                            g.GetComponent<AIPlayer>().playerRef
                                = Global.inst.players[i];
                            g.GetComponent<AIPlayer>().enabled = true;
                            Tutorial.ai.Add(g.GetComponent<AIPlayer>());
                        }
                        
                        i++;
                    } else 
                        addUnit(p, i, clone);
                    //   t.transform.up = p;
                    //  t.transform.RotateAround(t.transform.up, Random.Range(0, 360));
                    //   print("town " + t.transform.position + " at " + p);

                }

                float ang = Vector3.Angle(Vector3.forward, new Vector3(0, p.y, 1));
                if (ang < 25 && ang > 10)
                    // SAND
                    if (Random.value < .56f) {
                        clone.transform.GetChild(1).GetComponent<Renderer>().material =
                            sandMaterial;
                            /*
                            .SetTexture ("MainTex", sandTexture);
                        clone.transform.GetChild(1).GetComponent<Renderer>().material.
                            SetColor("Color", sandColor);
                            */
                        sand = true;
                    }
                if (Random.value < .125f && !town)
                {
                    // MOUNTAIN
                    clone = Instantiate(mtnPrototype, p, Quaternion.identity);
                    clone.transform.up = p;
                    clone.transform.RotateAround(clone.transform.up, Random.Range(0, 360));
                    clone.transform.parent = planet.transform;
                    mountain = true;
                } else
                {
                    GameObject cloneME = biomeTree(p, sand, snow);
                    if (cloneME != null && !town)
                    {
                        clone = Instantiate(cloneME, p, Quaternion.identity);
                        clone.transform.up = p;
                        clone.transform.RotateAround(clone.transform.up, Random.Range(0, 360));
                        clone.transform.parent = planet.transform;
                        forest = true;
                    }
                }
                Global.landscape[p] = new Global.LandscapeData(snow, sand, forest, mountain);
            }
            else
            {
                // water
                GameObject clone = Instantiate(waterPrototype, p, Quaternion.identity);
                clone.transform.up = p;
                float plain_rotation = Random.Range(0, 360);
                clone.transform.RotateAround(clone.transform.up, plain_rotation);
                clone.transform.parent = planet.transform;
                //clone.transform.GetChild(2).RotateAround(clone.transform.up, -plain_rotation);
                Global.grid[p] = clone;
                Global.landscape[p] = new Global.LandscapeData(Global.LandscapeData.Type.water);
                Global.units[p] = null;

            }
        }

        Global.inst.builNodes();
        Global.inst.setUpEpoch();

        centerCamera(home);

    }

    public GameObject biomeTree (Vector3 point, bool desert, bool snow )
    {
        if (snow) return null;
        float val = Random.value;
        if (desert)
        {
            if (val < .25f)
                return cactFusorestPrototype;
            if (val < .4f)
                return palmPoorForestPrototype;
        }
        float ang = Vector3.Angle(Vector3.forward, new Vector3(0, point.y, 1)); 
        if (ang < 30 )
            if (val < .4f)
                return palmForestPrototype;
        if ( ang < 55)
            if (val < .4f)
                return treeForestPrototype;
        if (val < .4f)
            return firForestPrototype;
        return null;
    }
    public float chances (float dist)
    {
        if (dist > 1f) return .03f;
        if (dist > .5) return .42f;
        else return .78f;

    }
    public float nearestKey (Vector3 point, List<Vector3> keys)
    {
        float dist = Vector3.Distance(point, keys[0]);
        foreach (Vector3 key in keys)
        {
            if (dist > Vector3.Distance(point, key))
                dist = Vector3.Distance(point, key);
        }
        return dist;
    }
    public List <Vector3> pickKeyPoints ()
    {
        List<Vector3> keys = new List<Vector3>();
        Vector3[] points = planet.GetComponent<MeshFilter>().mesh.vertices;
        for (int i=0; i<10; i++)
        {
            keys.Add(points[Random.Range(0, points.Length)]);
        }
        return keys;
    }

    public void centerCamera (Vector3 targetPosition)
    { 
        planet.transform.rotation = Quaternion.FromToRotation(targetPosition, Camera.main.transform.position);
        planet.transform.Rotate(15f,0,0);

    }

    public GameObject addUnit (Vector3 p, int playerId, GameObject cell)
    {
        GameObject clone = Instantiate(CharactersProxy.inst.barbarian, p, Quaternion.identity);
        clone.transform.up = p;
        clone.transform.parent = planet.transform;
        if (playerId < Global.inst.players.Count)
            clone.GetComponent<Unit>().owner = Global.inst.players[playerId];
        if (Global.unitDist == -1)
            Global.unitDist = Vector3.Distance(Vector3.zero, clone.transform.position);

        clone.GetComponent<Movement>().target_ = cell.GetComponentInChildren<DEBUG_OnClieckChangeBorder>().transform;

        Global.AddUnit(p, clone.GetComponent<Unit>());
        CharactersProxy.inst.Barbarian(clone);
        if (playerId == 0)
        {
            /*
            clone.GetComponent<Unit>().relatedIcon =
            Global.AddUnitHead(clone.transform.GetChild(0).GetChild(0).GetChild(0).GetChild(5).gameObject);
            clone.GetComponent<Unit>().relatedIcon.GetComponent<UIListMenuSelectableItem>().obj =
            clone.GetComponent<Selectable>();
            */
        }
        clone.transform.GetChild(1).GetChild(0).gameObject.GetComponent<Collider>().enabled = (true);
        return clone;
    }

  
}
