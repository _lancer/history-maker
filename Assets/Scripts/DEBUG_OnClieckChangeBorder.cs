﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DEBUG_OnClieckChangeBorder : MonoBehaviour
{

    public Player owner;
    // Start is called before the first frame update
    void Start()
    {
    }

    public void RemoveBorder()
    {
    }
    public void ShowBorderWhite()
    {
        
        
    }
    public void SetBackOriginal()
    {
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void add(Player player)
    {
        if (Global.landscape[transform.parent.transform.localPosition].type == Global.LandscapeData.Type.water)
            return;

        if (owner != null && owner != player)
        {
            if (player == Global.inst.localPlayer)
            {
                Global.inst.localPlayerEpoch.contact(owner);
            }

            owner.territory.Remove(transform.parent.transform.localPosition);
            owner.towns.Remove(transform.parent.transform.localPosition);

            if (Global.towns[transform.parent.transform.localPosition] != null)
            {
                
                Global.towns[transform.parent.transform.localPosition].SetActive(false);
                player.influence += 5;
                if (player == Global.inst.localPlayer)
                {
                    Global.inst.localPlayerEpoch.captureTown();
                }
            }
            
        }
        if(!player.territory.Contains(transform.parent.transform.localPosition))
            player.territory.Add(transform.parent.transform.localPosition);
        // player.merge(); 

        owner = player;
        transform.parent.GetChild(2).gameObject.SetActive(true);
        transform.parent.GetChild(2).gameObject.GetComponent<Renderer>().material
                            .SetColor("Color_A834058F", player.color);

        if (player == Global.inst.localPlayer)
        {
            Global.inst.localPlayerEpoch.discoverCell(transform.parent.transform.localPosition);
            Ui.inst.UpdateInfl();
        }
                            
    }


}
