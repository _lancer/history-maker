﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIListMenuSelectableItem : MonoBehaviour
{

    public static UIListMenuSelectableItem selected;
    public Selectable.I obj;

    public void OnClick ()
    {
        if (Ui.unselect != null)
            Ui.unselect.unselect();
        obj.Select();
        selected = this;
        centerCamera(Global.inst.planet.transform.InverseTransformPoint(obj.GetTransform().position));
        UIListMenuSelectableItem.selected.GetComponent<Image>().enabled = true;
    }

    public void centerCamera(Vector3 targetPosition)
    {
        Global.inst.planet.transform.rotation = Quaternion.FromToRotation(targetPosition, Camera.main.transform.position);
        Global.inst.planet.transform.Rotate(15f, 0, 0);

    }
}
