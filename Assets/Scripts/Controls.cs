﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class Controls : MonoBehaviour
{
    public GameObject planet;
    public GameObject camera;

    public Vector3 targetOffset = Vector3.zero;

    public float distance = 3.0f;
    public float distanceMin = 2.0f;
    public float distanceMax = 5.0f;

    public float xSpeed = 80.0f;
    public float ySpeed = 80.0f;
    public float zoomSpeed = 5.0f;

    float x = 0.0f;
    float y = 0.0f;

    private float xSpeedInertia = 0.0f;
    private float ySpeedInertia = 0.0f;
    public float xSpeedInertiaStart = 0.5f;
    private float xSpeedInertiaStop = 0.05f;
    private float ySpeedInertiaStart = 0.5f;
    public float ySpeedInertiaStop = 0.05f;

    private float touchK = 0.01f;

    private float xSpeedAcceleration = 0.01f;
    private float ySpeedAcceleration = 0.01f;

    void Start()
    {
        Application.targetFrameRate = 60; // TODO: move to Game Manager
        UpdatePosition();
    }

    // Camera Position Section

    void UpdateCamera()
    {
        x = 0.0f;
        y = 0.0f;
        UpdateRotation();
        UpdatePosition();
    }

    void UpdateRotation()
    {
        if (EventSystem.current.IsPointerOverGameObject(0) ||
            EventSystem.current.IsPointerOverGameObject(1) ||
            EventSystem.current.IsPointerOverGameObject(2))
            return;

        // Move
        if (Input.GetMouseButton(0))
        {
            if (Input.touchCount == 1)
            {
                var touch = Input.GetTouch(0);
                x += touch.deltaPosition.x * xSpeed * distance * xSpeedAcceleration * touchK;
                y += touch.deltaPosition.y * ySpeed * distance * ySpeedAcceleration * touchK;
                xSpeedInertia = Mathf.Lerp(xSpeedInertia, touch.deltaPosition.x * touchK, xSpeedInertiaStart);
                ySpeedInertia = Mathf.Lerp(ySpeedInertia, touch.deltaPosition.y * touchK, ySpeedInertiaStart);
            }
            else if (Input.touchCount == 0)
            {
                x += Input.GetAxis("Mouse X") * xSpeed * distance * xSpeedAcceleration;
                y += Input.GetAxis("Mouse Y") * ySpeed * distance * ySpeedAcceleration;
                xSpeedInertia = Mathf.Lerp(xSpeedInertia, Input.GetAxis("Mouse X"), xSpeedInertiaStart);
                ySpeedInertia = Mathf.Lerp(ySpeedInertia, Input.GetAxis("Mouse Y"), ySpeedInertiaStart);
            }
        }

        // Zoom
        if (Input.GetMouseButton(0) && Input.touchCount == 2)
        {
            var touchFirst = Input.GetTouch(0);
            var touchSecond = Input.GetTouch(1);
            var delta = (touchFirst.position - touchSecond.position).magnitude - (touchFirst.position - touchFirst.deltaPosition - touchSecond.position + touchSecond.deltaPosition).magnitude;
            distance = Mathf.Clamp(
                distance - delta * zoomSpeed * touchK,
                distanceMin,
                distanceMax
            );
        }
        else if (!Input.GetMouseButton(0) && Input.touchCount == 0)
        {
            distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel") * zoomSpeed, distanceMin, distanceMax);
        }
    }

    public void UpdatePosition()
    {
        xSpeedInertia = Mathf.Lerp(xSpeedInertia, 0, xSpeedInertiaStop);
        ySpeedInertia = Mathf.Lerp(ySpeedInertia, 0, ySpeedInertiaStop);

        x += xSpeedInertia * xSpeed * distance * xSpeedAcceleration;
        y += ySpeedInertia * ySpeed * distance * ySpeedAcceleration;

        RotateCamera(x, y);
        DistanceCamera(distance);
    }

    public void RotateCamera(float x, float y)
    {
        camera.transform.RotateAround(planet.transform.position, camera.transform.rotation * Vector3.up, x);
        camera.transform.RotateAround(planet.transform.position, camera.transform.rotation * Vector3.left, y);
    }

    public void DistanceCamera(float distance)
    {
        Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
        Vector3 position = camera.transform.rotation * negDistance + planet.transform.position;

        camera.transform.position = position + camera.transform.rotation * targetOffset;
    }

    // Input Section

    private Vector2 pointerClick = Vector2.zero;
    private bool isStartPointing = false;
    private bool isPointing = false;
    private float dragDistClickLimit = 15f;

    void Update()
    {
        if (EventSystem.current.IsPointerOverGameObject(0) ||
           EventSystem.current.IsPointerOverGameObject(1) ||
           EventSystem.current.IsPointerOverGameObject(2)
           )
            if (!(Input.touchCount > 0 && Input.GetTouch(0).phase != TouchPhase.Began))
                return;
        if (Input.GetMouseButton(0) && Input.touchCount == 0)
        {
            if (!isStartPointing)
            {
                isStartPointing = true;
                isPointing = true;
                pointerClick = Input.mousePosition;
            }
            if (isPointing)
            {
                if (Vector3.Distance(pointerClick, Input.mousePosition) > dragDistClickLimit)
                {
                    isPointing = false;
                }
            }
        }
        else if (Input.touchCount == 1)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                pointerClick = Input.GetTouch(0).position;
                isPointing = true;
            }

            if (Input.GetTouch(0).phase == TouchPhase.Moved && isPointing && Vector3.Distance(pointerClick, Input.GetTouch(0).position) > dragDistClickLimit)
            {
                pointerClick = Input.GetTouch(0).position;
                isPointing = false;
            }

            if (Input.GetTouch(0).phase == TouchPhase.Ended && isPointing)
            {
                HandleClick(Input.GetTouch(0).position);
                isPointing = false;
            }
        }
        else if (Input.touchCount > 1)
        {
            isPointing = false;
        }
        else if (isPointing)
        {
            HandleClick(Input.mousePosition);
            isPointing = false;
            isStartPointing = false;
        }
        else if (isStartPointing)
        {
            isStartPointing = false;
        }

        UpdateCamera();
    }

    public void HandleClick(Vector2 screen)
    {
        if (EventSystem.current.IsPointerOverGameObject(0) ||
            EventSystem.current.IsPointerOverGameObject(1) ||
            EventSystem.current.IsPointerOverGameObject(2))
            return;
        RaycastHit[] hits = Physics.RaycastAll(Camera.main.ScreenPointToRay(screen),
                5, LayerMask.GetMask("Selectable"));

        bool unit_selected = (Selectable.firstSelected != null);

        List<RaycastHit> hitsList = new List<RaycastHit>();
        hitsList.AddRange(hits);
        hitsList.Sort((x, y) => { return x.distance.CompareTo(y.distance); });

        Selectable whatewer = null;
        Selectable unit = null;
        Selectable cell = null;
        foreach (RaycastHit hit in hitsList)
        {
            
            if (mkFort.inProgress)
            {
                if (hit.collider.GetComponent<Selectable>().type == Selectable.Type.unit)
                    continue;

                mkFort.HireAt(hit.collider.GetComponent<Selectable>());
                break;
            }
            if (Build.inProgress)
            {
                if (hit.collider.GetComponent<Selectable>().type == Selectable.Type.unit)
                    continue;

                Build.BuildAt(hit.collider.GetComponent<Selectable>());
                break;
            }
            if (Hire.inProgress)
            {
                if (hit.collider.GetComponent<Selectable>().type == Selectable.Type.unit)
                    continue;

                Hire.HireAt(hit.collider.GetComponent<Selectable>());
                break;
            }
            if (MkBoat.inProgress)
            {
                if (hit.collider.GetComponent<Selectable>().type == Selectable.Type.unit)
                    continue;

                MkBoat.inst.HireAt(hit.collider.GetComponent<Selectable>());
                break;
            }
            else
            {
                if (whatewer == null) whatewer = hit.collider.GetComponent<Selectable>();
                if (hit.collider.GetComponent<Selectable>().type == Selectable.Type.unit)
                {
                    if (unit == null && Selectable.firstSelected != hit.collider.GetComponent<Selectable>())
                        unit = hit.collider.GetComponent<Selectable>();

                }
                else
                {
                    if (cell == null)
                        cell = hit.collider.GetComponent<Selectable>();
                }
            }
        }
        if (Build.inProgress || Hire.inProgress || MkBoat.inProgress) return;
        
        if (unit_selected)
        {
            if (Selectable.firstSelected
                .dropMe.Contains(cell.transform.parent.GetChild(3)))
                cell.Select();
            else
            {
                if (unit != null)
                    unit.Select();
                else
                {
                    if (Selectable.firstSelected.GetComponent<Unit>().currentMovement <= 0 ||
                         Unit.MovementCost(Global.landscape[cell.transform.parent.localPosition]) >
                         Selectable.firstSelected.GetComponent<Unit>().currentMovement)
                    {
                        SoundManager.inst.nope.Play();
                        InfoMessage.inst.Show("Army needs rest");
                    }
                    else
                    {
                        SoundManager.inst.nope.Play();
                        InfoMessage.inst.Show("Can't move there");
                    }
                }
            }
        } else
        {
            if (whatewer != null)
            whatewer.Select();
        }

    }
}
