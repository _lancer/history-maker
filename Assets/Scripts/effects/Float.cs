﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Float : MonoBehaviour
{
    public float speed;
    public float lifetime;

    public float currentTime;

    public Vector3 startPos;
    // Start is called before the first frame update
    void Start()
    {
        startPos = GetComponent<RectTransform>().anchoredPosition;
    }

    private void OnEnable()
    {
        GetComponent<RectTransform>().anchoredPosition = startPos;
    }
    private void OnDisable()
    {
        currentTime = 0;
    }

    public void rest()
    {
        GetComponent<RectTransform>().anchoredPosition = startPos;
        currentTime = 0;

    }
    // Update is called once per frame
    void Update()
    {
        currentTime += Time.deltaTime;

        if (currentTime > lifetime)
            gameObject.SetActive(false);
        Vector3 position = GetComponent<RectTransform>().anchoredPosition;
        GetComponent<RectTransform>().anchoredPosition = position + Vector3.up * speed * Time.deltaTime;
    }
}
