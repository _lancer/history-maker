﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoMessage : MonoBehaviour
{
    public static InfoMessage inst;

    public GameObject message;
    // Start is called before the first frame update
    void Start()
    {
        inst = this;
    }

    // Update is called once per frame
    public void Show (string text)
    {
        message.GetComponentInChildren<Text>().text = text;
        message.GetComponent<Float>().rest();
        message.SetActive(true);
    }

    public void KingdomLost (Player player)
    {

    }
}
