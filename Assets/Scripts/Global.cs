﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Global : MonoBehaviour
{

    public Text kingdomsRemian;


    public GameObject buildFortButton;
    public GameObject fortProto;
    // epoch
    public GameObject townIcon;
    public GameObject armyIcon;
    public GameObject ladIcon;
    public GameObject boatIcon;

    public Sprite[] townIcons;
    public Sprite[] armyIcons;
    public Sprite[] ladIcons;

    public Sprite[] boatIcons;

    public GameObject boatButton;
    public Text epochName;

    private void Start()
    {
        Global.inst.townIcon.GetComponent<UnityEngine.UI.Image>().sprite
            = Global.inst.townIcons[1];

        Global.inst.armyIcon.GetComponent<UnityEngine.UI.Image>().sprite
            = Global.inst.armyIcons[2];


    }

    public GameObject hireLadButton;

    public GameObject aiPrototype;

    public GameObject townPrototype;

    public static float unitDist = -1;
    public static float oneDayTimeUnit = 16;

    public static Global inst;
    public GameObject sun;

    public List<Player> players = new List<Player>();

    public Color[] playerColors;
    public GameObject[] borders;

    public Player localPlayer = new Player();

    public static Dictionary<Vector3, GameObject> grid = new Dictionary<Vector3, GameObject>();
    public static Dictionary<Vector3, LandscapeData> landscape = new Dictionary<Vector3, LandscapeData>();

    public static Dictionary<Vector3, List<Unit>> units = new Dictionary<Vector3, List<Unit>>();
    public static Dictionary<Vector3, GameObject> towns = new Dictionary<Vector3, GameObject>();
    public static Dictionary<Vector3, GameObject> forts = new Dictionary<Vector3, GameObject>();
    public static void AddUnit (Vector3 position, Unit unit)
    {
        if (unit.GetComponent<Army>() == null && unit.GetComponent<Boat>() == null)
            return;
        List<Unit> list = units[position];
        if (list == null)
        {
            list = new List<Unit>();
            units[position] = list;
        }
        list.Add(unit);
    }
    public static bool HasNoEnemies (Vector3 position, Player player)
    {
        if (units[position] == null) return true;
        foreach (Unit unit in units[position])
        {
            if (unit.owner !=null && unit.owner != player) return false;
        }
        return true;
    }

    public Color inactiveUIElem;
    public Color healthUIElem;
    public Color movementUIElem;

    public class LandscapeData
    {
        public enum Type
        {
            sand, snow, mountain, forest, low_dense_forest, plain, water
        }
        public Type type;

        public LandscapeData(Type type)
        {
            this.type = type;
        }

        public LandscapeData (bool snow, bool sand, bool forest, bool mountain)
        {
            if (mountain) type = Type.mountain;
            else if (snow) type = Type.snow;
            else if (sand && forest) type = Type.low_dense_forest;
            else if (forest) type = Type.forest;
            else type = Type.plain;
        }
    }
    public GameObject planet;

    public class node
    {
        public Vector3 point;
        public List<Vector3> near;

        public node(Vector3 point, List<Vector3> near)
        {
            this.point = point;
            this.near = near;
        }
    }
    public static Dictionary<Vector3, node> nodes = new Dictionary<Vector3, node>();
    // Start is called before the first frame update
    void Awake()
    {
        inst = this;
        
        grid = new Dictionary<Vector3, GameObject>();
    landscape = new Dictionary<Vector3, LandscapeData>();

    units = new Dictionary<Vector3, List<Unit>>();
    towns = new Dictionary<Vector3, GameObject>();
     forts = new Dictionary<Vector3, GameObject>();

        players = new List<Player>();

        Selectable.firstSelected = null;

        // movementRange.transform.position = Vector3.zero;
        localPlayer.color = playerColors[0];
        localPlayer.border = borders[0];
        localPlayer.border
            .GetComponent<Renderer>().material
                            .SetColor("Color_A834058F", localPlayer.color);
        players.Add(localPlayer);
        for (int i = 1; i<playerColors.Length; i++)
        {
            Player p = new Player();
            p.color = playerColors[i];
            p.border = borders[i];
            p.border
                .GetComponent<Renderer>().material
                                .SetColor("Color_A834058F", p.color);
            players.Add(p);
        }

    }

    public GameObject epochUI;
    public Epoch localPlayerEpoch = new Epoch();
    public void setUpEpoch()
    {
        localPlayerEpoch.ui = epochUI;
        localPlayerEpoch.updateUI();
    }


    float currTime = 0;

    public void CheckForVictory()
    {
        int lost = 0;
        foreach (Player player in players)
        {
            if (player.Lost())
                lost++;
        }
        kingdomsRemian.text = (players.Count - lost) + " kingdoms remain";
        if (lost == players.Count-1)
            Stats.inst.Show(!localPlayer.Lost());
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if (currTime > oneDayTimeUnit)
        {
            CheckForVictory();
            if (localPlayer.Lost())
                Stats.inst.Show(false);
            foreach (Player player in players)
            {
                player.influence += Economy.GetInfl(player);
                if (player == localPlayer)
                {
                    Stats.inst.money += Economy.GetInfl(player);
                }
              //  player.town.cityzens.Add(new Town.Cityzen());
            }
            currTime = 0;
            Ui.inst.UpdateInfl();

            localPlayerEpoch.dayily();
        }
        
        currTime += Time.deltaTime;
    }


    public void builNodes ()
    {
        foreach (Vector3 p in grid.Keys)
        {
            nodes[p] = new node(p, getNearest(p));
        }
    }

    public List<Vector3> getNearest(Vector3 position)
    {
        Dictionary<Vector3, GameObject> cells = Global.grid;

        List<Vector3> closest = new List<Vector3>();
        foreach (Vector3 p in cells.Keys)
        {
            if (closest.Count < 9)
            {
                closest.Add(p);
                closest.Sort((x, y) => {
                    return Vector3.Distance(x, position)
                        .CompareTo(Vector3.Distance(y, position));
                });
                continue;
            }

            if (Vector3.Distance(closest[8], position) > Vector3.Distance(p, position))
            {
                closest[8] = p;
                closest.Sort((x, y) => {
                    return Vector3.Distance(x, position)
                        .CompareTo(Vector3.Distance(y, position));
                });
            }
        }
        closest.Remove(position);
        return closest;

    }

    public GameObject headUI;
    public GameObject healthUI;
    public GameObject movwmentUI;

    public GameObject mainMenu;
    public GameObject armyMenu;
    public static void SetHeadUI (GameObject headSprite)
    {
      //  inst.headUI.SetActive(true);
    //    inst.healthUI.SetActive(true);
        inst.movwmentUI.SetActive(true);

        inst.mainMenu.SetActive(false);
        inst.armyMenu.SetActive(true);
        /*
        Image head = inst.headUI.GetComponent<Image>();
        head.sprite = headSprite.GetComponent<SpriteRenderer>().sprite;
        head.color = headSprite.GetComponent<SpriteRenderer>().color;
        for (int i = 0; i < headSprite.transform.childCount; i++) 
        {
            inst.headUI.transform.GetChild(i).gameObject.SetActive(true);
            Image img = inst.headUI.transform.GetChild(i).GetComponent<Image>();
            img.sprite = headSprite.transform.GetChild(i).GetComponent<SpriteRenderer>().sprite;
            img.color = headSprite.transform.GetChild(i).GetComponent<SpriteRenderer>().color;
            if (headSprite.transform.GetChild(i).GetComponent<SpriteRenderer>().sprite == null)
                inst.headUI.transform.GetChild(i).gameObject.SetActive(false);
        }
        */
    }

    public GameObject unitHeadPrototype;
    public GameObject leftScrollContent;
    public static GameObject AddUnitHead (GameObject headSprite)
    {
        GameObject clone = Instantiate(inst.unitHeadPrototype);
        clone.transform.SetParent (inst.leftScrollContent.transform, false);
        clone.SetActive(true);

        Image head = clone.GetComponent<Image>();
        head.sprite = headSprite.GetComponent<SpriteRenderer>().sprite;
        head.color = headSprite.GetComponent<SpriteRenderer>().color;
        for (int i = 0; i < headSprite.transform.childCount; i++)
        {
            clone.transform.GetChild(i).gameObject.SetActive(true);
            Image img = clone.transform.GetChild(i).GetComponent<Image>();
            img.sprite = headSprite.transform.GetChild(i).GetComponent<SpriteRenderer>().sprite;
            img.color = headSprite.transform.GetChild(i).GetComponent<SpriteRenderer>().color;
            if (headSprite.transform.GetChild(i).GetComponent<SpriteRenderer>().sprite == null)
                clone.transform.GetChild(i).gameObject.SetActive(false);
        }
        return clone;
    }

    public GameObject townContainer;
    public GameObject townIconPrototype;

    public static void AddTownButton(GameObject town)
    {
        return;
        /*
        GameObject clone = Instantiate(inst.townIconPrototype);
        clone.transform.SetParent(inst.townContainer.transform, false);
        clone.SetActive(true);
        clone.GetComponent<UIListMenuSelectableItem>().obj =
            town.GetComponent<Selectable.I>();
            */
    }

    public GameObject army;
    public Army AddArmy(Vector3 position, Player for_)
    {
        GameObject clone = Instantiate(army,
            Global.inst.planet.transform.TransformPoint(position), Quaternion.identity);

        clone.transform.up = Global.inst.planet.transform.TransformPoint(position);
        clone.transform.parent = Global.inst.planet.transform;
        clone.transform.localPosition = (position);

        CharactersProxy.inst.Barbarian(clone);
        clone.GetComponent<Unit>().owner = for_;

        clone.transform.GetChild(0).GetComponent<SpriteRenderer>().color = for_.color;

        clone.GetComponent<Movement>().target_ = Global.grid[position]
                .GetComponentInChildren<DEBUG_OnClieckChangeBorder>().transform;

        Global.AddUnit(position, clone.GetComponent<Unit>());

       for_.armies.Add(clone.GetComponent<Army>());

     
        return clone.GetComponent<Army>();
    }
}
