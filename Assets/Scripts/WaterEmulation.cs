﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterEmulation : MonoBehaviour
{
    // Start is called before the first frame update
    Material mat;
    Vector3 offset = Vector3.zero;
    public float speed = .03f;
    void Start()
    {
        mat = GetComponent<Renderer>().material;  
    }

    // Update is called once per frame
    void Update()
    {
        offset.x += Time.deltaTime * speed;
        mat.mainTextureOffset = offset;
    }
}
