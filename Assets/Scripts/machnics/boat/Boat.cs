﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boat : Unit
{
    private void OnDisable()
    {
        print("boat disabled");
    }
    public override void epochUpgrade(int epoch)
    {
        for (int i = 0; i < transform.GetChild(2).childCount; i++)
        {
            transform.GetChild(2).GetChild(i).gameObject.SetActive(i == epoch);
        }
    }

    public override bool CanMoveTo(Global.LandscapeData land, Vector3 point)
    {
        if (transporing == null) return false;
        if (currentMovement <= 0) return false;
        else if (land.type == Global.LandscapeData.Type.water) return true;
        else
            return base.CanMoveTo(land, point);
    }

    public bool Board (Army army)
    {
        if (transporing != null) return false;

        army.units.ForEach(u => u.gameObject.SetActive(false));
        army.transform.parent = transform;
        army.boarded = this;
        transporing = army;
        if (army.GetComponent<Unit>().owner == Global.inst.localPlayer)
            GetComponent<Selectable>().Select();
        return true;

    }

    public void Unboard (Transform target_)
    {
        transporing.transform.parent = Global.inst.planet.transform;
        transporing.units.ForEach(u => u.gameObject.transform.localPosition = transporing.transform.localPosition);
        transporing.units.ForEach(u => u.gameObject.SetActive(true));

        transporing.boarded = null;
        transporing.GetComponent<Movement>().target = target_;
        GetComponent<Selectable>().unselect();
        transporing.GetComponent<Selectable>().Select();
        transporing = null;


    }

}
