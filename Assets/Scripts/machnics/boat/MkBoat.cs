﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MkBoat : MonoBehaviour
{

    public GameObject prototype;

    public static MkBoat inst;
    // Start is called before the first frame update
    void Start()
    {
        inst = this;
    }

    public static bool inProgress = false;
    public void StartHiring()
    {
        Ui.inst.done();
        inProgress = true;

        findWaterCellsAroundCities();

        Ui.inst.doneButton.SetActive(true);
    }

    public void HireAt(Selectable selectable)
   {
        if (Global.inst.localPlayer.influence - Economy.GetBoatPrice() < 0)
        {
            InfoMessage.inst.Show("Not enough money");
            SoundManager.inst.nope.Play();
            return;
        }

        if (!inst.hideLater.Contains(selectable.transform.parent.GetChild(3)))
        {
            InfoMessage.inst.Show("Can't build there");

            SoundManager.inst.nope.Play(); return;
        }


        Global.inst.localPlayer.influence -= Economy.GetBoatPrice();
        Vector3 position =selectable.transform.parent.localPosition;

        GameObject clone = Instantiate(prototype,
            Global.inst.planet.transform.TransformPoint(position), Quaternion.identity);

        clone.transform.up = Global.inst.planet.transform.TransformPoint(position);
        clone.transform.parent = Global.inst.planet.transform;
        clone.transform.localPosition = (position);

        //clone.GetComponent<Unit>().owner = for_;

      //  clone.transform.GetChild(0).GetComponent<SpriteRenderer>().color = for_.color;

        clone.GetComponent<Movement>().target_ = Global.grid[position]
                .GetComponentInChildren<DEBUG_OnClieckChangeBorder>().transform;

        Global.AddUnit(position, clone.GetComponent<Unit>());

    //    for_.units.Add(clone.GetComponent<Unit>());
      //  for_.armies.Add(clone.GetComponent<Army>());
        // mountain fix
        if (Global.landscape[position].type == Global.LandscapeData.Type.mountain)
        {
            clone.transform.localPosition += clone.transform.localPosition.normalized * Movement.mtUp;
            clone.GetComponent<Movement>().onMountain = true;
        }
        Global.inst.localPlayerEpoch.buildShip();
        clone.GetComponent<Boat>().epochUpgrade(Global.inst.localPlayer.epoch);

        Ui.inst.UpdateInfl();
        SoundManager.inst.Boat.Play();
        //   return clone.GetComponent<Army>();
    }

    public void StopHiring()
    {
        foreach (Transform g in hideLater)
        {
            g.gameObject.SetActive(false);
        }
        hideLater.Clear();
        inProgress = false;
        Ui.inst.doneButton.SetActive(false);
    }

    List<Transform> hideLater = new List<Transform>();
    public void findWaterCellsAroundCities ()
    {
        foreach (Vector3 town in Global.inst.localPlayer.towns)
        {
            List<Vector3> near = Global.nodes[town].near;
            foreach (Vector3 n in near)
            {
                if (Global.landscape[n].type == Global.LandscapeData.Type.water)
                {

                    Global.grid[n].transform.GetChild(3).gameObject.SetActive(true);
                    hideLater.Add(Global.grid[n].transform.GetChild(3));
                }
            }
        }
    }
}
