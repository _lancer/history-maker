﻿using System.Collections.Generic;
using UnityEngine;

public class Fight_v2 : MonoBehaviour
{

    public float hitCooldown = .5f;
    public float currentCooldown = 0;

    bool onMarch_ = false;

    public int damage = 1;
    public bool onMarch
    {
        set
        {
            onMarch_ = value;
            if (value)
            {
                localTarget = null;
                currentCooldown = hitCooldown;
            }
        }
        get { return onMarch_; }
    }
    public bool inFight = false;

    public Unit thisUnit;
    public List<Unit> targets = new List<Unit>();

    Movement 
        mvmnt;

    public GameObject localTarget;

    
    AudioSource source;

    public AudioClip[] clips;
    float speed;
    // Start is called before the first frame update
    void Start()
    {
        hitCooldown += Random.value * .5f;
        source = GetComponent<AudioSource>();

        thisUnit = GetComponentInParent<Unit>();
        speed = GetComponentInParent<Movement>().speed;
        mvmnt = GetComponentInParent<Movement>();
    }
    /*
    public string name()
    {
        return thisUnit.owner.color + " " + thisUnit.GetHashCode();
    }
    void say (string what)
    {
        print ( name()  + " " + what );
    }
    */
    // Update is called once per frame
    void FixedUpdate()
    {
        if (localTarget != null )
        {
           // say("running to local tgt");
            Quaternion angleQ = Quaternion.FromToRotation(transform.parent.position, localTarget.transform.position);
            Vector3 angle = new Vector3(angleQ.x, angleQ.y, angleQ.z).normalized;

            transform.parent.RotateAround(Vector3.zero, Vector3.forward, angle.z * speed * Time.deltaTime);
            transform.parent.RotateAround(Vector3.zero, Vector3.right, angle.x * speed * Time.deltaTime);
            transform.parent.RotateAround(Vector3.zero, Vector3.up, angle.y * speed * Time.deltaTime);

            return;
        }
        if (inFight)
        {
            if (currentCooldown <= 0)
            {
                try
                {
                    Attack();
                }
                catch (System.Exception e) { }
                currentCooldown = hitCooldown + Random.value * .5f; ;
            } else
            {
                currentCooldown -= Time.deltaTime;
            }
        } else
        {
            if (!onMarch)
            {
                if (currentCooldown >= 0)
                    currentCooldown -= Time.deltaTime;
            }
        }
    }

    void Attack() {
        if (targets.Count == 0) PickLocalTarget();
        if (!targets[0].isActiveAndEnabled)
            targets.RemoveAt(0);

        //say("hits "+ targets[0].GetComponentInChildren<Fight_v2>().name());
        targets[0].lastDamager = thisUnit;
        targets[0].currentHealth -= damage;

        source.clip = clips[Random.Range(0, clips.Length)];
            source.Play();
        
        PushBack();
    }

    void PickLocalTarget ()
    {
        localTarget = mvmnt.target_.gameObject;
    }
    void PushBack ()
    {
        Transform transform_ = thisUnit.transform;
        Transform target_ = targets[0].transform;
        Quaternion angleQ = Quaternion.FromToRotation(transform_.localPosition,
            Vector3.MoveTowards(transform_.localPosition, target_.localPosition, -.003f));
        Vector3 angle = new Vector3(angleQ.x, angleQ.y, angleQ.z).normalized * .3f;

        transform_.RotateAround(Vector3.zero, Vector3.forward, angle.z);
        transform_.RotateAround(Vector3.zero, Vector3.right, angle.x);
        transform_.RotateAround(Vector3.zero, Vector3.up, angle.y);

    }
    public void OnTriggerEnter(Collider other)
    {
        Unit otherUnit = other.transform.parent.parent.GetComponent<Unit>();
        if (thisUnit.owner == otherUnit.owner) return;

        targets.Add ( other.transform.parent.parent.GetComponent<Unit>() );

       // say("meets " + otherUnit.GetComponentInChildren<Fight_v2>().name());
        localTarget = null;
    }

    public void OnTriggerExit(Collider other)
    {
        Unit otherUnit = other.transform.parent.parent.GetComponent<Unit>();
        if (thisUnit.owner == otherUnit.owner) return;

        targets .Remove (other.transform.parent.parent.GetComponent<Unit>() );

    }
}
