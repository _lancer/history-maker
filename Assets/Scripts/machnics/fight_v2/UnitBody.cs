﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitBody : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<UnitBody>() == null) return;
        Transform transform_ = this.transform.parent.parent;
        Transform target_ = other.transform.parent.parent;
        Quaternion angleQ = Quaternion.FromToRotation(transform_.localPosition, 
            Vector3.MoveTowards(transform_.localPosition, target_.localPosition, -.003f));
        Vector3 angle = new Vector3(angleQ.x, angleQ.y, angleQ.z).normalized * .3f;
        if(angle == Vector3.zero)
        {
            transform_ = this.transform.parent.parent;
             angleQ = Quaternion.FromToRotation(transform_.localPosition,
                transform_.right);
             angle = new Vector3(angleQ.x, angleQ.y, angleQ.z).normalized * .3f;

        }

        transform_.RotateAround(Vector3.zero, Vector3.forward, angle.z);
        transform_.RotateAround(Vector3.zero, Vector3.right, angle.x);
        transform_.RotateAround(Vector3.zero, Vector3.up, angle.y);

    }
}
