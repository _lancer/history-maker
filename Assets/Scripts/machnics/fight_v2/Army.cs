﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Army : MonoBehaviour
{

    public Boat boarded = null;

    public GameObject uiElem;

    public TextMesh unitCount;

    public List<Unit> units = new List<Unit>();

    public void AddUnit (Unit unit)
    {
        units.Add(unit);
        unit.armyRef = this;
        unitCount.text = units.Count + "";
    }

    public int UnitsCount() { return units.Count; }
    bool inFight_ = false;
    public bool inFight
    {
        get { return inFight_; }
        set { if (uiElem!=null ) uiElem.SetActive(!value); inFight_ = value; units.ForEach(u => u.GetComponentInChildren<Fight_v2>().inFight = value); }
    }

    public void DeadReported (Unit u)
    {
        if (GetComponent<Boat>() != null)
            return;
        units.Remove(u);
        unitCount.text = units.Count + "";
        if (units.Count == 0)
        {
            Global.units[GetComponent<Movement>().target_.parent.localPosition].Remove(GetComponent<Unit>());
            Global.units[GetComponent<Movement>().target_.parent.localPosition].ForEach(a =>
           {
               a.GetComponent<Army>().verifyFighting_status();
           });
            GetComponent<Unit>().owner.armies.Remove(this);
            gameObject.SetActive(false);
        }
    }

    public void verifyFighting_status ()
    {
        if (Global.HasNoEnemies(GetComponent<Movement>().target_.parent.localPosition, GetComponent<Unit>().owner))
        {
            GetComponent<Selectable>().ShowRange();
            GetComponent<Movement>().CaptureCell();
            GetComponent<Army>().inFight = false;
        }
        else
        {
            GetComponent<Army>().inFight = true;
        }
    }
   // public void
}
