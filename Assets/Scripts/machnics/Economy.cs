﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Economy 
{

    public static int GetInfl(Player player)
    {
        /*
        if (player == Global.inst.localPlayer)
            Debug.Log("terr " + player.territory.Count + " un " + player.units.Count + " armies " 
            + player.armies.Count + " towns " + player.towns.Count);
            */
        int income = 10;
        income += player.territory.Count;
        /*
        foreach (Town.Building b in town.buildings)
        {
            income += b.infl();
        }
        /*
        foreach (Town.Workplace w in town.workplaces)
        {
            income -= w.maint();
        }
        */
        income -= player.units.Count * 2;
        income -= player.armies.Count * 3;

        income += (player.towns.Count-1) * 4;
        return income;
    }

    public static int GetTownPrice (Player player)
    {
        return 5 + (player.towns.Count - 1) * 2 + (player.towns.Count - 2);
    }

    public static int GetArmyPrice ()
    {
        return 7;
    }

    public static int GetUnitPrice()
    {
        return 5;
    }

    public static int GetBoatPrice()
    {
        return 20;
    }
}
