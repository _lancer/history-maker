﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Town : MonoBehaviour, Selectable.I
{
    // town building should modify town presense
    public class Cityzen
    {
        public Building home;
        public Workplace workplace;
    }
    public class Building
    {
        public Workplace workplace;

        public virtual int infl()
        {
            return 0;
        }
    }
    public Transform GetTransform()
    {
        return transform;
    }
    public void unselect()
    {
        UIListMenuSelectableItem.selected.GetComponent<Image>().enabled = false;
        transform.GetChild(0).GetChild(1).gameObject.SetActive(false);
        Ui.inst.doneButton.SetActive(false);
       
    }
    public void Select()
    {
        transform.GetChild(0).GetChild(1).gameObject.SetActive(true);
       Ui. inst.buildings_menu.SetActive(true);
        Ui.inst.doneButton.SetActive(true);
        Ui.unselect = this;
    }
    public class Workplace
    {
        public virtual int maint()
        {
            return 0;
        }
    }


    public Player owner;
    // Start is called before the first frame update
    void Start()
    {
        
    }

}
