﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selectable : MonoBehaviour, Selectable.I
{

    public interface I
    {
         void unselect();
         void Select();
        Transform GetTransform();
    }

    public Transform GetTransform ()
    {
        return transform;
    }
    public enum Type
    {
        unit,
        land
    }

    public static Selectable firstSelected;
    static Selectable secondSelected;
    public Type type;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void unselect ()
    {
        if (GetComponent<Army>() != null)
            GetComponent<Army>().uiElem = null;
        Clean();
        GetComponent<Unit>().healthTracker = null;
        GetComponent<Unit>().movementTracker = null;
        GetComponent<Unit>().HighlightIcon(false);
        transform.GetChild(0).GetChild(0).gameObject.SetActive(false);
        Ui.inst.doneButton.SetActive(false);
        Global.inst.headUI.SetActive(false);
        Global.inst.healthUI.SetActive(false);
        Global.inst.movwmentUI.SetActive(false);



        Global.inst.mainMenu.SetActive(true);
        Global.inst.armyMenu.SetActive(false);
        firstSelected = null;
    }
    public void Select()
    {
        if (type == Type.unit)
        {
            if (GetComponent<Unit>().owner != Global.inst.localPlayer) 
                if (!(GetComponent<Boat>() != null && GetComponent<Boat>().transporing.GetComponent<Unit>().owner ==
                    Global.inst.localPlayer))
                    return;
            if (Ui.unselect != null)
            {
                Ui.unselect.unselect();

            }
            if (GetComponent<Army>() != null && GetComponent<Army>().boarded != null)
            {

                GetComponent<Army>().boarded.GetComponent<Selectable>().Select();
                return;
            }
            firstSelected = this;
            Ui.unselect = firstSelected;
            Ui.inst.doneButton.SetActive(true);

            GetComponent<Unit>().healthTracker = Global.inst.healthUI;
            GetComponent<Unit>().movementTracker = Global.inst.movwmentUI;

            GetComponent<Unit>().refreshTrackers();
            GetComponent<Unit>().HighlightIcon(true);

            transform.GetChild(0).GetChild(0).gameObject.SetActive(true);

            Global.SetHeadUI(null);

            if (GetComponent<Army>() != null)
            {
                GetComponent<Army>().uiElem = Global.inst.hireLadButton;
                Global.inst.hireLadButton.SetActive(!GetComponent<Army>().inFight 
                    && GetComponent<Movement>().movementDone && GetComponent<Army>().boarded == null);

            }
          //  print("selected");
            ShowRange();

            SoundManager.inst.PlayClick();
           // print(closest_.Count);
           //  if (closest_ == null || closest_.Count == 0 ) getNearest();
            return;
        }
        if (firstSelected != null)
        {
            if (!firstSelected.GetComponent<Selectable>().dropMe.Contains(transform.parent.GetChild(3)))
            {
                if (firstSelected.GetComponent<Unit>().currentMovement <= 0)
                {
                    SoundManager.inst.nope.Play();
                    InfoMessage.inst.Show("Army tired");
                } else
                {
                    SoundManager.inst.nope.Play();
                    InfoMessage.inst.Show("Can't move there");
                }
                return;
            }
            if (transform.parent.GetChild(3) == firstSelected.GetComponent<Selectable>().same) return;
            firstSelected.GetComponent<Movement>().target = transform;
            firstSelected.GetComponent<Movement>().toPay
                = Unit.MovementCost(Global.landscape[transform.parent.localPosition]);


           // if ()
        }
        
    }
    public List<Transform> dropMe = new List<Transform>();
    public Transform same;
    public void ShowRange() {
        if (firstSelected != this) return;
        Vector3 position = GetComponent<Movement>().target_.transform.localPosition;

        Transform cur;
        try
        {
            cur = Global.grid[position].transform.GetChild(3);
            // print(GetComponent<Movement>().target_);
        }
        catch (System.Exception e)
        {
            position = GetComponent<Movement>().target_.transform.parent.localPosition;
            cur = Global.grid[position].transform.GetChild(3);

        }

        same = cur;
        dropMe.Add(cur);
        List<Vector3> near = Global.nodes[position].near;
        cur.gameObject.SetActive(true);

        foreach (Vector3 p in near)
        {
            if (Global.grid[p] != null)
            {
                // area availability movement points
                if (!GetComponent<Unit>().CanMoveTo(Global.landscape[p], p))
                {
                    continue;
                }
                Global.grid[p].transform.GetChild(3).gameObject.SetActive(true);
                dropMe.Add(Global.grid[p].transform.GetChild(3));
            }
        }
    }

    public void HideRange ()
    {

    }
    public void Clean ()
    {
        foreach (Transform t in dropMe)
        {
            t.gameObject.SetActive(false);
        }
        dropMe.Clear();
        same = null;
    }

    
}
