﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mkFort : MonoBehaviour
{
    public List<GameObject> hideLater = new List<GameObject>();
    public static mkFort inst;
    private void Start()
    {
        inst = this;
    }

    // hindle situations when borders changed
    public static bool inProgress = false;
    public void StartHiring()
    {
        Ui.inst.done();
        inProgress = true;
        foreach (Vector3 point in Global.inst.localPlayer.territory)
        {
            if (Global.inst.localPlayer.forts.Contains(point))
                continue;
            GameObject go = Global.grid[point];
            hideLater.Add(Global.grid[point].transform.GetChild(3).gameObject);
            Global.grid[point].transform.GetChild(3).gameObject.SetActive(true);
        }
        Ui.inst.doneButton.SetActive(true);
    }

    public static void HireAt(Selectable selectable)
    {
        // TODO change price
        /*
        if (Global.inst.localPlayer.influence - Economy.GetArmyPrice() < 0)
            return;
        Global.inst.localPlayer.influence -= Economy.GetArmyPrice();
        */
        
        if (inst.hideLater.Contains(selectable.transform.parent.GetChild(3).gameObject))
        {
            print("fort");
            AddFort(Global.inst.localPlayer, selectable.transform.parent.localPosition);
            inst.StopHiring();
            inst.StartHiring();
        }
    }

    public void StopHiring()
    {
        foreach (GameObject g in hideLater)
        {
            g.SetActive(false);
        }
        hideLater.Clear();
        inProgress = false;
        Ui.inst.doneButton.SetActive(false);
    }

    public static void AddFort (Player player, Vector3 at)
    {
        /*
        if (player.influence - Economy.GetTownPrice(player) < 0)
            return;

        player.influence -= Economy.GetTownPrice(player);
        */

        GameObject t = Instantiate(Global.inst.fortProto, at, Quaternion.identity);
        t.transform.parent = Global.grid[at].transform;
        t.transform.localPosition = Vector3.zero;
        t.transform.localRotation = Quaternion.identity;
        player.forts.Add(at);
        Ui.inst.UpdateInfl();
        if (Global.landscape[at].type == Global.LandscapeData.Type.mountain)
        {
            //   print(t.transform.localPosition);
            t.transform.localPosition += Global.grid[at].transform.localPosition.normalized
                * (Movement.mtUp * 5);
            // print(t.transform.localPosition);
        }

        Global.forts[at] = t;
        if (player == Global.inst.localPlayer)
        {
            Global.inst.localPlayerEpoch.buildTown();
        }
        /*
        for (int i = 0; i < t.transform.childCount; i++)
        {
            t.transform.GetChild(i).gameObject.SetActive(i == player.epoch);
        }
        */
    }
}
