﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Build : MonoBehaviour
{
    public List<GameObject> hideLater = new List<GameObject>();
    public static Build inst;
    private void Start()
    {
        inst = this;
    }
    // hindle situations when borders changed
    public static bool inProgress = false;
    public void StartBuilding()
    {
        Ui.inst.done();
        inProgress = true;
        foreach (Vector3 point in Global.inst.localPlayer.territory)
        {
            if (Global.inst.localPlayer.towns.Contains(point))
                continue;
            GameObject go = Global.grid[point];
            hideLater.Add(Global.grid[point].transform.GetChild(3).gameObject);
            Global.grid[point].transform.GetChild(3).gameObject.SetActive(true);
        }
        Ui.inst.doneButton.SetActive(true);
    }

    public static void BuildAt(Selectable selectable)
    {
        if (inst.hideLater.Contains(selectable.transform.parent.GetChild(3).gameObject))
        {
            Ui.inst.BuildTown(selectable.transform.parent.localPosition, Global.inst.localPlayer);

            inst.StartBuilding();
        } else
        {
            InfoMessage.inst.Show("Can't build there");

            SoundManager.inst.nope.Play();

        }
    }

    public void StopBuilding()
    {
        foreach (GameObject g in hideLater)
        {
            g.SetActive(false);
        }
        hideLater.Clear();
        inProgress = false;
        Ui.inst.doneButton.SetActive(false);
    }
}
