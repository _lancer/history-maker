﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    Animator Animator;
    public Transform target_;

    public Transform Origin;

    public AIPlayer ai;

    public AudioSource source;

    public int toPay;
    Transform flip;
    public static float mtUp = .08f;
    public Transform target { set
        {
            if (GetComponent<Boat>() != null 
                && Global.landscape[value.parent.localPosition].type != Global.LandscapeData.Type.water)
            {
                destrTown.Play();
                GetComponent<Boat>().Unboard(value);
                return;
            }
            if (GetComponent<Army>()!=null)
            {
                if (GetComponent<Army>().boarded != null)
                {
                    GetComponent<Army>().boarded.Unboard(value);
                    destrTown.Play();
                    GetComponent<Army>().boarded = null;
                    return;
                }
            }
            if (target_ != value && GetComponentInChildren<Fight_v2>() != null)
                GetComponentInChildren<Fight_v2>().onMarch = true;
            Origin = target_;
            if (GetComponent<Army>() != null || GetComponent<Boat>() != null)
            {
                if (source != null)
                    source.Play();
                if (Global.units[target_.parent.localPosition] != null)
                    Global.units[target_.parent.localPosition].Remove(GetComponent<Unit>());
            }
            if (GetComponent<Army>() != null)
                if (GetComponent<Army>().uiElem != null) GetComponent<Army>().uiElem.SetActive(false);
            movementDone = false;
            target_ = value;
            GetComponent<Selectable>().Clean();
            //  print("set");
            if (Animator != null)
            Animator.SetBool("My run", true);

            if (GetComponent<Army>() != null)
            {
                GetComponent<Army>().units.ForEach(u => u.GetComponent<Movement>().target = value);
            }
            if (onMountain)
            {
                transform.localPosition = transform.localPosition.normalized * Global.unitDist;
                onMountain = false;
            }
            
        } 
    }
    public float speed = 30;
    // Start is called before the first frame update
    void Start()
    {
        Animator = GetComponentInChildren<Animator>();       // target_ = transform;
        if (GetComponent<Boat>() == null)
            flip = transform.GetChild(0);
        else
            flip = transform.GetChild(2);
    }
    // Update is called once per frame
    void Update()
    {
        if (target_ == null) return;
        if (movementDone) return;
        if (Vector3.Distance(transform.position, target_.position) < (.05f + (onMountain? mtUp : 0) ) )
        {
            //   Animator.Play("Idle", 0);
            if (Animator != null)
            Animator.SetBool("My run", false);
            MovementDone();
            return;
        }
        Quaternion angleQ = Quaternion.FromToRotation(transform.position, target_.position);
        Vector3 angle = new Vector3 (angleQ.x, angleQ.y, angleQ.z)    .normalized;

        transform.RotateAround(Vector3.zero, Vector3.forward, angle.z * speed * Time.deltaTime);
        transform.RotateAround(Vector3.zero, Vector3.right, angle.x * speed * Time.deltaTime);
        transform.RotateAround(Vector3.zero, Vector3.up, angle.y * speed * Time.deltaTime);

        if (GetComponent<Army>() != null) return;
        Vector3 scale = flip.localScale;
        if (Vector3.SignedAngle (target_.position, transform.position, 
            Camera.main.transform.up) > 0  )
        {
            scale.x = -Mathf.Abs (scale.x);
        } else
        {
            scale.x = Mathf.Abs(scale.x);
        }

        flip.localScale = scale;
    }
    public bool movementDone = true;

    public bool onMountain = false;

    public AudioSource destrTown;
    public bool CaptureCell()
    {

        if (Global.HasNoEnemies(target_.parent.localPosition, GetComponent<Unit>().owner))
        {
            if (target_.gameObject.GetComponent<DEBUG_OnClieckChangeBorder>() != null)
                target_.gameObject.GetComponent<DEBUG_OnClieckChangeBorder>().add(GetComponent<Unit>().owner);
            else
                target_.parent.GetComponentInChildren<DEBUG_OnClieckChangeBorder>().add(GetComponent<Unit>().owner);
            /*
            try
            {
                if (Global.towns[target_.parent.localPosition] != null)
                    destrTown.Play();
            }
            catch (System.Exception e) { }
            */
            return true;
        }
        return false;
    }
    void MovementDone ()
    {
        if (movementDone) return;

       
        if (GetComponent<Army>() != null)
            if (GetComponent<Army>().uiElem != null) GetComponent<Army>().uiElem.SetActive(true);
        if (GetComponent<Army>() != null || GetComponent<Boat>() != null)
            Global.AddUnit(target_.parent.localPosition, GetComponent<Unit>());
        GetComponent<Unit>().currentMovement -= toPay;
        movementDone = true;
        GetComponent<Selectable>().Clean();
        // print(target_);
        if (GetComponent<Army>() != null)
        {
            CaptureCell();
            if (source != null)
                source.Stop();
        }
       // mass battle over here
       if (Global.HasNoEnemies(target_.parent.localPosition, GetComponent<Unit>().owner))
            GetComponent<Selectable>().ShowRange();
       else
        {
            if (GetComponent<Army>() != null)
            {
                GetComponent<Army>().inFight = true;
                Global.units[target_.parent.localPosition].ForEach(u => u.GetComponent<Army>().inFight = true );
            }
        }
        if ( GetComponentInChildren<Fight_v2>() != null)
            GetComponentInChildren<Fight_v2>().onMarch = false;

        // mountain fix
        if (Global.landscape[target_.parent.localPosition].type == Global.LandscapeData.Type.mountain)
        {
            transform.localPosition += transform.position.normalized * mtUp;
            onMountain = true;
        }
        if (ai != null)
            ai.ArmyMovementDone(GetComponent<Army>());

        if (GetComponent<Army>() != null && Global.landscape[target_.parent.localPosition].type == Global.LandscapeData.Type.water)
        {
            destrTown.Play();
            foreach (Unit unit in Global.units[target_.parent.localPosition] )
            {
                if (unit.GetComponent<Boat>().Board(GetComponent<Army>())) return;
            }
        }
    }

    public void RunBack ()
    {
        if (Origin == null) target = Global.grid[GetComponent<Unit>().owner.home]
                .GetComponentInChildren<DEBUG_OnClieckChangeBorder>().transform;
        GetComponent<Unit>().currentMovement -= toPay;
        movementDone = false;
        target = Origin;
        Origin = target_;
    }
}
