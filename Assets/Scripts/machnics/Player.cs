﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player
{

   // public int epoch = 0;
    public int epoch = 1;

    public List<Vector3> territory = new List<Vector3>();
    public List<Vector3> towns = new List<Vector3>();

    public List<Vector3> forts = new List<Vector3>();

    public List<Unit> units = new List<Unit>();
    public List<Army> armies = new List<Army>();

    //capital
    public Town town;

    public Vector3 home;

    public Color color;

    public GameObject border;

    [HideInInspector]
    public int influence = 20;

    public bool Lost ()
    {
        return territory.Count == 0;
    }

}
