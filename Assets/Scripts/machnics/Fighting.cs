﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fighting : MonoBehaviour
{
    public Unit thisUnit;
    public Animator Animator;

    float delay = 0.5f;
    public float hitTime = 1;
    // Start is called before the first frame update
    void Start()
    {
        thisUnit = GetComponentInParent<Unit>();
    }

    /*
    // fight - 
    each unit attacks 3 times 1 per sec
    attaker has penality .5 sec for first attack

        defender is the one who staying on cell (movement origin = movement taeget);

    if attaker not dead and defender not dead - attacker returns to start cell
    if attacker dead nothing happens
    if defender dead cell changes ovner

        units should look at each other

        glorify fightning
         amount of damage
         splat sprite

    */
    // Update is called once per frame

    bool inFight = false;
    int delayRequired = 0;
    int hits = 3;
    public bool moved = false;
    int damage = 1;
    Unit target;

    float currentTime = 0;
    void FixedUpdate()
    {
        if (!inFight) return;
        if(target.currentHealth <= 0)
        {
            inFight = false;
            hits = 3;
           if( !GetComponentInParent<Movement>().CaptureCell())
                GetComponentInParent<Movement>().RunBack();
           else
                GetComponent<Selectable>().ShowRange();

            return;
        }
        if (hits == 0)
        {
            inFight = false;
            GetComponentInParent<Movement>().RunBack();
            hits = 3;
            return;
        }
        if (hits == 3 && moved)
        {
            delayRequired = 1;
        }
        else
            delayRequired = 0;

        currentTime += Time.deltaTime;
        if (currentTime > hitTime + delayRequired * delay)
        {
         //   Animator.SetTrigger( "Slash");
            currentTime = 0;
            target.currentHealth -= damage;
            hits--;
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        Unit otherUnit = other.transform.parent.parent.GetComponent<Unit>();
        if (thisUnit.owner == otherUnit.owner) return;

        target = other.transform.parent.parent.GetComponent<Unit>();
        inFight = true;

    }
}
