﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Unit : MonoBehaviour
{

    public Army armyRef;

    public Player owner;

    public int maxMovement = 3;
    public int maxHealth = 5;

    public int currentHealth_ = 5;
    public int currentMovement_ = 3;

    float restorationTime;

    public GameObject relatedIcon;

    public Town.Cityzen cityzenRef;

    public virtual void epochUpgrade (int epoch)
    {

    }

    public void HighlightIcon (bool flag)
    {
        if (relatedIcon != null)
            relatedIcon.GetComponent<Outline> ().enabled = flag;
    }

    public void UpdateMovementStatus (int currentMovement)
    {
        if (relatedIcon != null)
        {
            relatedIcon.transform.GetChild(10).gameObject.SetActive(currentMovement == maxMovement);
            transform.GetChild(0).GetChild(4).gameObject.SetActive(currentMovement == maxMovement);

        }
    }
    private void FixedUpdate()
    {
        if (restorationTime > Global.oneDayTimeUnit/2) {
            currentMovement = maxMovement;
            restorationTime = 0;
            if (GetComponentInChildren<Fighting>()!=null)
                GetComponentInChildren<Fighting>().moved = false;
            if (Selectable.firstSelected == GetComponent<Selectable>())
                GetComponent<Selectable>().Clean();
                GetComponent<Selectable>().ShowRange();

            if (currentHealth_ <= 0)
            {
                if (GetComponent<Boat>() != null) { }
                else
                {
                    gameObject.SetActive(false);
                    if (armyRef != null)
                        armyRef.DeadReported(this);
                }
            }
        }
        restorationTime += Time.deltaTime;
    }
    public int currentHealth
    {
        set
        {
            currentHealth_ = value;
            if (healthTracker != null)
                UpdateTracker(healthTracker, currentHealth_, Global.inst.healthUIElem);
            if (currentHealth_ <= 0)
                die();
        }
        get
        {
            return currentHealth_;
        }
    }
    public int currentMovement
    {
        set
        {
            currentMovement_ = value;
            restorationTime = 0;
            UpdateMovementStatus(value);
            if (movementTracker != null)
                UpdateTracker(movementTracker, currentMovement_, Global.inst.movementUIElem);
        }
        get
        {
            return currentMovement_;
        }
    }
    [HideInInspector]
    public GameObject healthTracker;
    [HideInInspector]
    public GameObject movementTracker;
    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        currentMovement = maxMovement;
    }

    void UpdateTracker (GameObject uiComponent, int currentValue, Color activeColor)
    {
        int i = 0;
        foreach (Image img in uiComponent.GetComponentsInChildren<Image> ())
        {
            if (i < currentValue)
                img.color = activeColor;
            else
                img.color = Global.inst.inactiveUIElem;
            i++;
        }
    }

    public Unit lastDamager;
    public void refreshTrackers()
    {
        UpdateTracker(healthTracker, currentHealth_, Global.inst.healthUIElem);
        UpdateTracker(movementTracker, currentMovement_, Global.inst.movementUIElem);
    }
    bool dead = false;
    public void die()
    {
        dead = true;
        try
        {
            GetComponent<Movement>().enabled = false;
            GetComponent<Selectable>().enabled = false;
            GetComponentInChildren<Fight_v2>().enabled = false;
            GetComponentInChildren<UnitBody>().enabled = false;
        }
        catch (System.Exception e) { }

        if (owner == Global.inst.localPlayer)
            Stats.inst.unitsLost += 1;

        if (lastDamager.owner == Global.inst.localPlayer)
            Stats.inst.unitsKilled += 1;

        owner.units.Remove(this);
        
        armyRef.DeadReported(this);
        gameObject.SetActive(false);


    }
    public virtual bool CanMoveTo(Global.LandscapeData land, Vector3 point)
    {
        if (land == null) return false;
        if (land.type == Global.LandscapeData.Type.water && currentMovement > 0) return CheckBoat(point);
        if (land.type == Global.LandscapeData.Type.mountain && currentMovement < 3) return false;
        else if ((land.type == Global.LandscapeData.Type.snow ||
            land.type == Global.LandscapeData.Type.forest) && currentMovement < 2) return false;
        else if (currentMovement < 1) return false;
        else return true;
    }

    public Army transporing = null;
    public bool CheckBoat (Vector3 point)
    {
        if (Global.units[point] == null) return false;
        foreach (Unit unit in Global.units[point] )
        {
            if (unit.GetComponent<Boat>() != null
                && unit.GetComponent<Boat>().transporing == null) return true;
        }
        return false;
    }
    public static int MovementCost (Global.LandscapeData land)
    {
        if (land == null) return 1;
        if (land.type == Global.LandscapeData.Type.mountain) return 3;
        else if ((land.type == Global.LandscapeData.Type.snow ||
            land.type == Global.LandscapeData.Type.forest)) return 2;
        else return 1;
    }
}
