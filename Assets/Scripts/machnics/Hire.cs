﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hire : MonoBehaviour
{
    
    public List<GameObject> hideLater = new List<GameObject>();
    public static Hire inst;
    private void Start()
    {
        inst = this;
    }
    // hindle situations when borders changed
    public static bool inProgress = false;
    public void StartHiring()
    {
        Ui.inst.done();
        inProgress = true;
        foreach (Vector3 point in Global.inst.localPlayer.towns)
        {
            GameObject go = Global.grid[point];
            hideLater.Add(Global.grid[point].transform.GetChild(3).gameObject);
            Global.grid[point].transform.GetChild(3).gameObject.SetActive(true);
        }
        Ui.inst.doneButton.SetActive(true);
    }

    public static void HireAt (Selectable selectable)
    {
        if (Global.inst.localPlayer.influence - Economy.GetArmyPrice() < 0)
        {
            SoundManager.inst.nope.Play();
            InfoMessage.inst.Show("Not enough money");

            return;
        }

        if (inst.hideLater.Contains(selectable.transform.parent.GetChild(3).gameObject))
        {

            Global.inst.localPlayer.influence -= Economy.GetArmyPrice();

            Army a= Global.inst.AddArmy(selectable.transform.parent.localPosition, Global.inst.localPlayer);
            Unit u = Ui.inst.AddUnit(selectable.transform.parent.localPosition);
            a.AddUnit(u);
            u.owner = a.GetComponent<Unit>().owner;
            u.armyRef = a;

           Global.inst.localPlayerEpoch. hire();

            Hire.inst.StopHiring();

            a.GetComponent<Selectable>().Select();

            SoundManager.inst.army.Play();

            Stats.inst.unitsHired += 1;
        } else
        {
            InfoMessage.inst.Show("Can't hire there");

            SoundManager.inst.nope.Play();
        }
    }

    public void StopHiring()
    {
        foreach (GameObject g in hideLater)
        {
           g.SetActive(false);
        }
        hideLater.Clear();
        inProgress = false;
        Ui.inst.doneButton.SetActive(false);
    }
}
